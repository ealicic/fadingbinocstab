function varargout = nanout(kill, varargin)

out = varargin;
for ii = 1:length(out)
    tmp = out{ii};
    tmp(kill) = nan;
    out{ii} = tmp;
end
varargout = out;

end