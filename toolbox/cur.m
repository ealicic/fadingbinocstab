function curvatur=cur(x,y)
    dx = gradient(x);
    ddx = gradient(dx);
    dy = gradient(y);
    ddy = gradient(dy);

    num = dx .* ddy - ddx .* dy;
    denom = dx .* dx + dy .* dy;
    denom = sqrt(denom);
    denom = denom .* denom .* denom;
    curvatur = num ./ denom;
    curvatur(denom < 0) = NaN;
end