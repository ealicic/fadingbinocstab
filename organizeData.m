%{
This has been updated to reflect the addition of various conditions added
to the fading paradigm as of 12/11/2019. Will not function for data
collected prior to this data as written (to do: update code to have this
functionality)
%}

clear all
subject = 'EA';
path = '/Users/EminAlicic/Box Sync/Binocular Fading/Data';
pathtodata = fullfile(path, subject);

read_Data = 0; %if set to 1 all data is read, if 0 previous mat file is loaded
if (read_Data == 1)
    [data, files] = fading_readdata(pathtodata,fading_CalList(),true); %set to true to save in mat file and then use load.
else
    load(fullfile(pathtodata, 'results.mat'));
end

%em  = preprocessBinocularEM(data);

EYEC = nan(1,size(data.user,1));
STABC = nan(1,size(data.user,1));
filt_COND = nan(1,size(data.user,1)); %used to be filt_COND

for tr = 1:size(data.user,1)
    
    %retrieve and allocate my data
    eye_d = double(data.user{tr}.EyeCond);
    stab_d = double(data.user{tr}.StabCond);
    EYEC(tr) = eye_d;
    STABC(tr) = stab_d;
    raw_COND(tr,:) = [data.user{tr}.EyeCond data.user{tr}.StabCond];
   
    %filter data based on eye cond and stab cond
    if eye_d == 1 && stab_d == 0 %right; no stab
        filt_COND(tr) = 1;
    elseif eye_d == 1 && stab_d == 1 %right; right stab
        filt_COND(tr) = 2;
    elseif eye_d == 1 && stab_d == 2 %right; left stab
        filt_COND(tr) = 1;
    elseif eye_d == 1 && stab_d == 3 %right; binoc stab
        filt_COND(tr) = 4;
    elseif eye_d == 1 && stab_d == 4 %right; version stab
        filt_COND(tr) = 5;
    elseif eye_d == 1 && stab_d == 5 %right; vergence stab
        filt_COND(tr) = 6;
    elseif eye_d == 1 && stab_d == 6 %right; horiz stab
        filt_COND(tr) = 7;
    elseif eye_d == 1 && stab_d == 7 %right; vert stab
        filt_COND(tr) = 8;
    elseif eye_d == 2 && stab_d == 0 %left; no stab
        filt_COND(tr) = 9;
    elseif eye_d == 2 && stab_d == 1 %left; right stab
        filt_COND(tr) = 9;
    elseif eye_d == 2 && stab_d == 2 %left; left stab
        filt_COND(tr) = 11;
    elseif eye_d == 2 && stab_d == 3 %left; binoc stab
        filt_COND(tr) = 12;
    elseif eye_d == 2 && stab_d == 4 %left; version stab
        filt_COND(tr) = 13;
    elseif eye_d == 2 && stab_d == 5 %left; vergence stab
        filt_COND(tr) = 14;
    elseif eye_d == 2 && stab_d == 6 %left; horiz stab
        filt_COND(tr) = 15;
    elseif eye_d == 2 && stab_d == 7 %left; vert stab
        filt_COND(tr) = 16;
    elseif eye_d == 3 && stab_d == 0 %binoc; no stab
        filt_COND(tr) = 17;
    elseif eye_d == 3 && stab_d == 1 %binoc; right stab
        filt_COND(tr) = 18;
    elseif eye_d == 3 && stab_d == 2 %binoc; left stab
        filt_COND(tr) = 19;
    elseif eye_d == 3 && stab_d == 3 %binoc; binoc stab
        filt_COND(tr) = 20;
    elseif eye_d == 3 && stab_d == 4 %binoc; version stab
        filt_COND(tr) = 21;
    elseif eye_d == 3 && stab_d == 5 %binoc; vergence stab
        filt_COND(tr) = 22;
    elseif eye_d == 3 && stab_d == 6 %binoc; horiz stab
        filt_COND(tr) = 23;
    elseif eye_d == 3 && stab_d == 7 %binoc; vert stab
        filt_COND(tr) = 24;
    else
        filt_COND(tr) = nan; %everything else
    end
    
    
    % trigger points
    RT(tr,:) = any(data.stream00{tr}.data == 7);  %right trigger
    
    LT(tr,:) = any(data.stream01{tr}.data == 7);  %left trigger
    
    % RIGHT
    tp_r = find(data.stream00{tr}.data == 7,1);
    
    if isempty(tp_r)
        FadeTM_total(tr,:) = nan;
    else
        FadeTM_total(tr,:) = double(data.stream00{tr}.ts(tp_r));
    end
    
    % LEFT
    tp_l = find(data.stream01{tr}.data == 7,1);
    
    if isempty(tp_l)
        FadeTM_partial(tr,:) = nan;
    else
        FadeTM_partial(tr,:) = double(data.stream01{tr}.ts(tp_l));
    end
    
end

%% Set title strings, allocate data based on condition
for fading_cond = 1:2 % changed 'p' to 'fading_cond' to make it clear that it's referring to the fading condition (partial or total)
    
    if fading_cond == 1
        tstr =  'Total Fading';
        ystr = 'Time from Stimulus Onset to First Report of Total Fading (ms)';
        DATA = FadeTM_total;
        
    elseif fading_cond == 2
        tstr = 'Partial Fading';
        ystr =  'Time from Stimulus Onset to First Report of Partial Fading (ms)';
        DATA = FadeTM_partial;
    end
end

%% plotting f(condition) = # trials

conds = [sum(filt_COND ==1) sum(filt_COND ==2) sum(filt_COND ==3) ...
    sum(filt_COND ==4) sum(filt_COND ==5) sum(filt_COND ==6) sum(filt_COND ==7) ...
    sum(filt_COND ==8) sum(filt_COND ==9) sum(filt_COND ==10) sum(filt_COND ==11) ...
    sum(filt_COND ==12) sum(filt_COND ==13) sum(filt_COND ==14) sum(filt_COND ==15) ...
    sum(filt_COND ==16) sum(filt_COND ==17) sum(filt_COND ==18) sum(filt_COND ==19) ...
    sum(filt_COND ==20) sum(filt_COND ==21) sum(filt_COND ==22) sum(filt_COND ==23) sum(filt_COND ==24)];

bar(conds); hold on
xticks([1:24]);
xtickangle(45);
xlim([1 24]);
xlabel('Conditions');
ylabel('# Trials');
title(['Number of trials by condition for subject: ', subject,' Trials = ',num2str(length(data.user))]);
set(gca,'box','off','tickdir','out');
set(gca,'XtickLabel',{'R; Norm','R; Right Stab','R; Left Stab','R; Binoc Stab',...
    'R; Version Stab','R; Vergence Stab','R; Horiz Stab','R; Vert Stab',...
    'L; Norm','L; Right Stab','L; Left Stab','L; Binoc Stab',...
    'L; Version Stab','L; Vergence Stab','L; Horiz Stab','L; Vert Stab',...
    'B; Norm','B; Right Stab','B; Left Stab','B; Binoc Stab',...
    'B; Version Stab','B; Vergence Stab','B; Horiz Stab','B; Vert Stab'})