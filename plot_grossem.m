%% plot_grossem.m
%{
This function takes the gross count of all microsaccades, saccades, and
drifts, and allows you to plot them onto bar graphs
%}

%% Load and preprocess data

% Clear and close everything
clear all
close all

% Set the path to data
subject = 'MAC';
data = ('Users/EminAlicic/Desktop/AP Lab Files/Stereo Fading/Data/temp/results.mat');
load('Users/EminAlicic/Desktop/AP Lab Files/Stereo Fading/Data/temp/results.mat');

em = preprocessBinocularEM(data);
%% Separate data in appropriate matrices
clear  nMsaccade* nSaccade* nDrift* 
ALL_COND = nan(1,size(data.user,1));
for tr = 1:size(data.user,1)
    
    %retrieve my data
    eye_d = double(data.user{tr}.EyeCond);
    stab_d = double(data.user{tr}.StabCond);
    ms_d = em{tr,1}.microsaccades.start;
    
    %store my data
    EYEC(tr) = eye_d;
    STABC(tr) = stab_d;
    COND(tr,:) = [data.user{tr}.EyeCond data.user{tr}.StabCond];
    
    %separate data for the box plot
    if eye_d == 0 && stab_d == 0 
        ALL_COND(tr) = 2;
    elseif eye_d == 0 && stab_d == 3
        ALL_COND(tr) = 1;
    elseif eye_d == 1 && stab_d == 0
        ALL_COND(tr) = 4;
    elseif eye_d == 1 && stab_d == 3
        ALL_COND(tr) = 3;
    elseif eye_d == 2 && stab_d == 0
        ALL_COND(tr) = 6;
    elseif eye_d == 2 && stab_d == 3
        ALL_COND(tr) = 5;
    else
        ALL_COND(tr) = nan;
    end
    
    % Right Eye
    nMsaccade1(tr) = length(em{tr,1}.microsaccades.start);
    nSaccade1(tr) = length(em{tr,1}.saccades.start);
    nDrift1(tr) = length(em{tr,1}.drifts.start);
    
    % Left Eye
    nMsaccade2(tr) = length(em{tr,2}.microsaccades.start);
    nSaccade2(tr) = length(em{tr,2}.saccades.start);
    nDrift2(tr) = length(em{tr,2}.drifts.start);
end

%% Create the plots

% Depending on what you want to analyze, change these values to 0 or 1.
analyzeSaccades = 1;
analyzeMsaccades = 0;
analyzeDrifts = 0;

% Saccades
if analyzeSaccades == 1
    sac2_1 = sum(nSaccade1(ALL_COND == 2));
    sac1_1 = sum(nSaccade1(ALL_COND == 1));
    sac4_1 = sum(nSaccade1(ALL_COND == 4));
    sac3_1 = sum(nSaccade1(ALL_COND == 3));
    sac6_1 = sum(nSaccade1(ALL_COND == 6));
    sac5_1 = sum(nSaccade1(ALL_COND == 5));
    
    [a1,~,c1] = unique(ALL_COND);
    subplot(1,2,1) % Right Eye
    A1 = accumarray(c1(:),nSaccade1(:));
    bar(a1(:),A1(:))
    title('Total Saccades Per Condition'); hold on
    ylabel('Saccade Count in Right Eye');
    xlabel('Conditions');
    set(gca,'box','off','tickdir','out');
    set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
    xlim([0.5 6.5]);
    
    subplot(1,2,2) % Left Eye
    A2 = accumarray(c1(:),nSaccade2(:));
    bar(a1(:),A2(:))
    title('Total Saccades Per Condition'); hold on
    ylabel('Saccade Count in Left Eye');
    xlabel('Conditions');
    set(gca,'box','off','tickdir','out');
    set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
    xlim([0.5 6.5]);
    
else
    disp('You did not choose to analyze saccades');
end

% Microsaccades
if analyzeMsaccades == 1
    msac2_1 = sum(nMsaccade1(ALL_COND == 2));
    msac1_1 = sum(nMsaccade1(ALL_COND == 1));
    msac4_1 = sum(nMsaccade1(ALL_COND == 4));
    msac3_1 = sum(nMsaccade1(ALL_COND == 3));
    msac6_1 = sum(nMsaccade1(ALL_COND == 6));
    msac5_1 = sum(nMsaccade1(ALL_COND == 5));
    
    subplot(1,2,1) % Right Eye
    [a1,~,c1] = unique(ALL_COND);
    A1 = accumarray(c1(:),nMsaccade1(:));
    bar(a1(:),A1(:))
    title('Total Microsaccades Per Condition'); hold on
    ylabel('Microsaccade Count in Right Eye');
    xlabel('Conditions');
    set(gca,'box','off','tickdir','out');
    set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
    xlim([0.5 6.5]);
    
    subplot(1,2,2) % Left Eye
    A2 = accumarray(c1(:),nMsaccade2(:));
    bar(a1(:),A2(:))
    title('Total Microsaccades Per Condition'); hold on
    ylabel('Microsaccade Count in Left Eye');
    xlabel('Conditions');
    set(gca,'box','off','tickdir','out');
    set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
    xlim([0.5 6.5]);
    
else
    disp('You did not choose to analyze microsaccades');
end

% Drifts
if analyzeDrifts == 1
    msac2_1 = sum(nDrift1(ALL_COND == 2));
    msac1_1 = sum(nDrift1(ALL_COND == 1));
    msac4_1 = sum(nDrift1(ALL_COND == 4));
    msac3_1 = sum(nDrift1(ALL_COND == 3));
    msac6_1 = sum(nDrift1(ALL_COND == 6));
    msac5_1 = sum(nDrift1(ALL_COND == 5));
    
    subplot(1,2,1) % Right Eye
    [a1,~,c1] = unique(ALL_COND);
    A1 = accumarray(c1(:),nDrift1(:));
    bar(a1(:),A1(:))
    title('Total Driftss Per Condition'); hold on
    ylabel('Drift Count in Right Eye');
    xlabel('Conditions');
    set(gca,'box','off','tickdir','out');
    set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
    xlim([0.5 6.5]);
    
    subplot(1,2,2) % Left Eye
    A2 = accumarray(c1(:),nDrift2(:));
    bar(a1(:),A2(:))
    title('Total Drifts Per Condition'); hold on
    ylabel('Drift Count in Left Eye');
    xlabel('Conditions');
    set(gca,'box','off','tickdir','out');
    set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
    xlim([0.5 6.5]);
    
else
    disp('You did not choose to analyze drifts');
end

