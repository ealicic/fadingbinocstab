%{
This contains all of the stabilization conditions used in the binocular
fading experiment.

NOTE: referred to as various different variables in some parts of older code. **EA - go
through and change this as development of code continues**

NOTE: 'Y' refers to vergence, 'Z' refers to version
%}

BINOC_NORM = 1; %binocular presentation, no stab
BINOC_BSTAB  = 2; %binocular presentation, binoc stab
RIGHT_NORM = 3; %RIGHT PRESENTATION, NO STAB
RIGHT_BSTAB = 4; %RIGHT PRESENTATION, BINOC STAB
LEFT_NORM = 5; %LEFT PRESENTATION, NO STAB
LEFT_BSTAB = 6; %LEFT PRESENTATION, BINOC STAB

BINOC_LSTAB = 7; %BINOC PRESENTATION, RIGHT STAB
BINOC_RSTAB = 8; %BINOC PRESENTATION, LEFT STAB
RIGHT_RSTAB = 9; %RIGHT PRESENTATION, RIGHT STAB
RIGHT_LSTAB = 10; %RIGHT PRESENTATION, LEFT STAB
LEFT_RSTAB = 11; %LEFT PRESENTATION, RIGHT STAB
LEFT_LSTAB = 12; %LEFT PRESENTATION, LEFT STAB

BINOC_YSTAB = 13; %BINOC PRESENTATION, VERGENCE ('Y') STAB
BINOC_ZSTAB = 14; %BINOC PRESENTATION, VERSION ('Z') STAB
RIGHT_YSTAB = 15;
RIGHT_ZSTAB = 16;
LEFT_YSTAB = 17;
LEFT_ZSTAB = 18;

BINOC_HSTAB = 19; %BINOC PRESETATION, HORIZ. STAB
BINOC_VSTAB = 20; %BINOC PRESENTATION, VERT. STAB
RIGHT_HSTAB = 21;
RIGHT_VSTAB = 22;
LEFT_HSTAB = 23;
LEFT_VSTAB = 24;

stabConditionNames = {'Binoc Normal','Binoc Full Stab','Right Normal',...
    'Right Full Stab','Left Normal','Left Full Stab','Binoc Right Stab',...
    'Binoc Left Stab','Right Right Stab','Right Left Stab','Left Right Stab',...
    'Left Left Stab','Binoc Vergence Stab','Binoc Version Stab','Right Vergence Stab',...
    'Right Version Stab','Left Vergence Stab','Left Version Stab','Binoc Horizontal Stab',...
    'Binoc Vertical Stab','Right Horizontal Stab','Right Vertical Stab','Left Horizontal Stab',...
    'Left Vergence Stab'};

nStabCond = length(stabConditionNames);

