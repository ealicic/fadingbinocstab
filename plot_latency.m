%% plot_latency.m
%{
This function plots the latency (time from stimulus onset to first report
of total or partial fading)

Emin Alicic, 2019, AP Lab
%}

%% Preprocess data

clear all
close all

% Set the path to data
subject = 'MAC';
data = ('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat');
load('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat')
[em] = fading_CalList(data); %EIS read

% Set figure path

fig_path = '/Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Figures/MAC';
sub_fig_path = fullfile(fig_path);
if (~isfolder(sub_fig_path))
    mkdir(sub_fig_path)
end

%% Create matrices for data based on conditions

EYEC = nan(1,size(data.user,1));
STABC = nan(1,size(data.user,1));
ALL_COND = nan(1,size(data.user,1));


for tr = 1:size(data.user,1)
    
    %retrieve my data
    eye_d = double(data.user{tr}.EyeCond);
    stab_d = double(data.user{tr}.StabCond);
    ms_d = em{tr,1}.microsaccades.start;
    
    %store my data
    EYEC(tr) = eye_d;
    STABC(tr) = stab_d;
    COND(tr,:) = [data.user{tr}.EyeCond data.user{tr}.StabCond];
    
    %separate data for the box plot
    if eye_d == 0 && stab_d == 0
        ALL_COND(tr) = 2;
    elseif eye_d == 0 && stab_d == 3
        ALL_COND(tr) = 1;
    elseif eye_d == 1 && stab_d == 0
        ALL_COND(tr) = 4;
    elseif eye_d == 1 && stab_d == 3
        ALL_COND(tr) = 3;
    elseif eye_d == 2 && stab_d == 0
        ALL_COND(tr) = 6;
    elseif eye_d == 2 && stab_d == 3
        ALL_COND(tr) = 5;
    else
        ALL_COND(tr) = nan;
    end
    
    
    RT(tr,:) = any(data.stream00{tr}.data == 7);  %right trigger
    %RT.ts = data.stream01{1,1}.ts;
    LT(tr,:) = any(data.stream01{tr}.data == 7);  %left trigger
    
    tp_r = find(data.stream00{tr}.data == 7,1);
   
    % 'find' finds the first index where there is a 7 in data.stream00{tr}.data
    if isempty(tp_r)
        FadeTM_total(tr,:) = nan;
    else
        FadeTM_total(tr,:) = double(data.stream00{tr}.ts(tp_r));
        % 'double' converts to double precision array
        
        % Finds when the most recent microsaccade occurred after pressing
        % the right trigger in both eyes
        RTmsidx1(tr,:) = double(em{tr,1}.microsaccades.start(tp_r));
        RTmsidx2(tr,:) = double(em{tr,2}.microsaccades.start(tp_r));
    end
    
    tp_l = find(data.stream01{tr}.data == 7,1);
    if isempty(tp_l)
        FadeTM_partial(tr,:) = nan;
    else
        FadeTM_partial(tr,:) = double(data.stream01{tr}.ts(tp_l));
        
        % Finds when the most recent microsaccade occurred after pressing
        % the left trigger in both eyes
        LTmsidx1(tr,:) = double(em{tr,1}.microsaccades.start(tp_l));
        LTmsidx2(tr,:) = double(em{tr,2}.microsaccades.start(tp_l));
    end
    
end

%% Initialize plots, clear figures
figure('units','in','position',[0 0 1 3/4 ] *7)
clf

%% Set title strings, allocate data based on condition
for fading_cond = 1:2 % changed 'p' to 'fading_cond' to make it clear that it's referring to the fading condition (partial or total)
    
    if fading_cond == 1
        tstr =  'Total Fading';
        ystr = 'Time from Stimulus Onset to First Report of Total Fading (ms)';
        DATA = FadeTM_total;
        
    elseif fading_cond == 2
        tstr = 'Partial Fading';
        ystr =  'Time from Stimulus Onset to First Report of Partial Fading (ms)';
        DATA = FadeTM_partial;
    end
    
    % Specify the two eyes and stabilization conditions
    % need to go back in, replicate this and create a separate variable as
    % I did with STABC for example up above to fix the boxplot
    
    eyes = [1 2]; clear u
    for eye = 0:2
        
        if eye ~= 0 % Left and right eyes
            
            % Defining normal conditions in L and R eyes
            normal =  COND(:,1) == eye & ...
                (COND(:,2) == 3 | COND(:,2) == eyes(eyes~=eye));
            
            % Defining stabilized conditions in L and R eyes
            stab = COND(:,1) == eye & ...
                (COND(:,2) == eye | COND(:,2) == 0);
            
            % 'u' takes the mean across both normal and stab data
            u(eye+1,1) =  nanmean(DATA(normal));
            u(eye+1,2) =  nanmean(DATA(stab));
            
            
            % 'n' takes the number of array elements for normal and stab
            n(eye+1,1) =  numel(DATA(normal));
            n(eye+1,2) =  numel(DATA(stab));
            
        else % Binocular
            normal =  COND(:,1) == eye & ...
                (COND(:,2) == 3) ;
            
            stab = COND(:,1) == eye & ...
                ( COND(:,2) == 0);
            
            u(eye+1,1) =  nanmean(DATA(normal));
            u(eye+1,2) =  nanmean(DATA(stab));
            
            n(eye+1,1) =  numel(DATA(normal));
            n(eye+1,2) =  numel(DATA(stab));
        end
    end
    
    
    %% Bar plot
    
    figure(1)
    [p,TB] = anovan(DATA,{EYEC, STABC}, 'display', 'off'); %anova window off
    subplot(1,2,fading_cond);
    errY = 0.05*u; % 5% error
    barwitherr(errY,u); hold on
    set(gca,'box','off','tickdir','out');
    set(gca,'XtickLabel',{'Binocular','Right Eye','Left Eye'})
    legend('Normal','Stabilized')
    ylabel(ystr);
    xlabel('Conditions');
    for eye = 1:3
        for stab = 1:2
            text(eye + -0.2 + (stab-1)/4,0, sprintf('%u',n(eye,stab)),...
                'VerticalAlignment','bottom')
        end
    end
    text(min(xlim),5,'Trial N =','VerticalAlignment','bottom','HorizontalAlignment','Left')
    str1 = sprintf('Eye Condition: F(%d, %d) = %.2f, p = %u\nStabilization: F(%d, %d) = %.2f, p = %u',...
        TB{2,3}, TB{4,3}, TB{2,6}, TB{2,7},TB{3,3}, TB{4,3}, TB{3,6}, TB{3,7});
    title({'MAC Fading Report  ' tstr});
    hold off
    
    
    %% Box plot
    
    figure(2)
    subplot(1,2,fading_cond);
    boxplot(DATA(:),ALL_COND);
    hold on
    set(gca,'box','off','tickdir','out');
    set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
    ylabel(ystr);
    xlabel('Conditions');
    str = sprintf('Eye Condition: F(%d, %d) = %.2f, p = %u\nStabilization: F(%d, %d) = %.2f, p = %u',...
        TB{2,3}, TB{4,3}, TB{2,6}, TB{2,7},TB{3,3}, TB{4,3}, TB{3,6}, TB{3,7});
    title({tstr; str});
    hold off
end   
 













