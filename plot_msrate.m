%% plot_msrate.m
%{
This code creates boxplots for the rate of microsaccades as a function of
experimental conditions with ANOVA statistics.

Emin Alicic, 2019, AP Lab.
%}

%%
clear all
subject = 'temp';
path = ('Users/EminAlicic/Desktop/AP Lab Files/Stereo Fading/Data');
pathtodata = fullfile(path, subject);

read_Data = 0; %if set to 1 all data is read, if 0 previous mat file is loaded
if (read_Data == 1)
    [data, files] = fading_readdata(pathtodata,fading_CalList(),true); %set to true to save in mat file and then use load.
else
    load(fullfile(pathtodata, 'results.mat'));
end

em = preprocessBinocularEM(data);



%% Create matrices for data based on conditions

EYEC = nan(1,size(data.user,1));
STABC = nan(1,size(data.user,1));
ALL_COND = nan(1,size(data.user,1));
NSWCH = nan(1,size(data.user,1));
allem = nan(1,size(data.user,1));

for tr = 1:size(data.user,1)
    
    %retrieve my data
    eye_d = double(data.user{tr}.EyeCond);
    stab_d = double(data.user{tr}.StabCond);
    ms_d = em{tr,1}.microsaccades.start;
    
    %store my data
    EYEC(tr) = eye_d;
    STABC(tr) = stab_d;
    COND(tr,:) = [data.user{tr}.EyeCond data.user{tr}.StabCond];
    
    %separate data for the box plot
    if eye_d == 0 && stab_d == 0
        ALL_COND(tr) = 2;
    elseif eye_d == 0 && stab_d == 3
        ALL_COND(tr) = 1;
    elseif eye_d == 1 && stab_d == 0
        ALL_COND(tr) = 4;
    elseif eye_d == 1 && stab_d == 3
        ALL_COND(tr) = 3;
    elseif eye_d == 2 && stab_d == 0
        ALL_COND(tr) = 6;
    elseif eye_d == 2 && stab_d == 3
        ALL_COND(tr) = 5;
    else
        ALL_COND(tr) = nan;
    end
    
    %binning all stab and norm conditions together
    if eye_d == 0 && stab_d == 3
        all_norm(tr) = 1;
    elseif eye_d == 1 && stab_d == 3
        all_norm(tr) = 1;
    elseif eye_d == 2 && stab_d == 3
        all_norm(tr) = 1;
    else
        all_norm(tr) = nan;
    end
    
    if eye_d == 0 && stab_d == 0
        all_stab(tr) = 2;
    elseif eye_d == 1 && stab_d == 0
        all_stab(tr) = 2;
    elseif eye_d == 2 && stab_d == 0
        all_stab(tr) = 2;
    else
        all_stab(tr) = nan;
    end
       
end
     

%% Sort data
clear  nMsaccade* nSaccade* nDrift* trialDur* rate_ms* NEWrate_ms* 

Fs = 1000;

% Figures out eye movement counts, as well as rates
for e = 1:length(data.user)
   
    nMsaccade1(e) = length(em{e,1}.microsaccades.start);
    nSaccade1(e) = length(em{e,1}.saccades.start);
    nDrift1(e) = length(em{e,1}.drifts.start);
    trialDur1(e) = em{e,1}.samples/Fs;
    rate_ms1(e) = nMsaccade1(e) / (trialDur1(e)); %unclear which rates are best A.T.M.
    rate_s1(e) = nSaccade1(e) / (trialDur1(e));
    rate_d1(e) = nDrift1(e) / (trialDur1(e));
    NEWrate_ms1(e) = (nSaccade1(e) + nMsaccade1(e)) / (trialDur1(e));
    
 
    nMsaccade2(e) = length(em{e,2}.microsaccades.start);
    nSaccade2(e) = length(em{e,2}.saccades.start);
    nDrift2(e) = length(em{e,2}.drifts.start);
    trialDur2(e) = em{e,2}.samples/Fs;
    rate_ms2(e) = nMsaccade2(e) / (trialDur2(e));
    rate_s2(e) = nSaccade2(e) / (trialDur2(e));
    rate_d2(e) = nDrift2(e) / (trialDur2(e));
    NEWrate_ms2(e) = (nSaccade2(e) + nMsaccade2(e)) / (trialDur2(e));

end

% Put all of that info into variables
clear em1 em2
em1 = [nMsaccade1(:) nSaccade1(:) trialDur1(:) rate_ms1(:)  nDrift1(:) NEWrate_ms1(:) rate_s1(:) rate_d1(:)];
em2 = [nMsaccade2(:) nSaccade2(:) trialDur2(:) rate_ms2(:)  nDrift2(:) NEWrate_ms2(:) rate_s2(:) rate_d2(:)];

clear dat
dat = [em1(:,4), em2(:,4), em1(:,7), em1(:,8), em2(:,7), em2(:,8)];

% ANOVA stats
clear TBEM*
[~,TBEM1] = anovan(dat(~isnan(ALL_COND),1),{EYEC(~isnan(ALL_COND)), STABC(~isnan(ALL_COND))}, 'display', 'off','model','interaction'); %anova window off
[~,TBEM2] = anovan(dat(~isnan(ALL_COND),2),{EYEC(~isnan(ALL_COND)), STABC(~isnan(ALL_COND))}, 'display', 'off','model','interaction'); %anova window off


%% Create the boxplots

msavgRate = (dat(:,1) + dat(:,2))/ 2;
savgRate = (dat(:,3) + dat(:,5))/2;
davgRate = (dat(:,4) + dat(:,6))/2;

avgRate_stab_ms = mean(msavgRate(~isnan(all_stab)));
avgRate_norm_ms = mean(msavgRate(~isnan(all_norm)));

avgRate_stab_s = mean(savgRate(~isnan(all_stab)));
avgRate_norm_s = mean(savgRate(~isnan(all_norm)));

avgRate_stab_d = mean(davgRate(~isnan(all_stab)));
avgRate_norm_d = mean(davgRate(~isnan(all_norm)));

%plot([avgRate], [all_norm]);


%plot([avgRate], [all_stab]);

%hold on;
%plot(avgRate(~isnan(all_norm)));

stabplot_ms = [1, avgRate_norm_ms];
normplot_ms = [2, avgRate_stab_ms];


stabplot_s = [1, avgRate_norm_s];
normplot_s = [2, avgRate_stab_s];


stabplot_d = [1, avgRate_norm_d];
normplot_d = [2, avgRate_stab_d];

plot([normplot_ms(1) stabplot_ms(1)], [normplot_ms(2), stabplot_ms(2)]); hold on;
%plot([normplot_s(1) stabplot_s(1)], [normplot_s(2), stabplot_s(2)]);
%plot([normplot_d(1) stabplot_d(1)], [normplot_d(2), stabplot_d(2)]);
xlim([0.5 2.5]);
ylim([-1 2.5]);
set(gca,'XtickLabel',{[],'Unstabilized',[],'Stabilized',[]});
set(gca,'TickDir','out');
title('Rate of microsaccades');
%h = ttest(~isnan(all_norm(rate_ms1)), ~isnan(all_stab));
%sigstar({[1,2]},[0.05]);





%{
%Right eye
figure(); cla; hold on
subplot(1,2,1)
boxplot(dat(:,1),ALL_COND);
set(gca,'box','off','tickdir','out');
set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
ylabel('Rate of Microsaccades (Microsaccades/se Across Trials)');
xlabel('Conditions');
str = sprintf('Eye Condition: F(%d, %d) = %.2f, p = %.3f\nStabilization: F(%d, %d) = %.2f, p = %.3f\nInteraction:  F(%d, %d) = %.2f, p = %.3f',...
    TBEM1{2,3}, TBEM1{5,3}, TBEM1{2,6}, TBEM1{2,7},TBEM1{3,3}, TBEM1{5,3}, TBEM1{3,6}, TBEM1{3,7}, TBEM1{4,3}, TBEM1{5,3}, TBEM1{4,6}, TBEM1{4,7});
tstr1 = 'Right Eye';
title({tstr1; str});

%Left eye
subplot(1,2,2)
boxplot(dat(:,2),ALL_COND);
set(gca,'box','off','tickdir','out');
set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
ylabel('Rate of Microsaccades (Microsaccades/s Across Trials');
xlabel('Conditions');
str = sprintf('Eye Condition: F(%d, %d) = %.2f, p = %.3f\nStabilization: F(%d, %d) = %.2f, p = %.3f\nInteraction:  F(%d, %d) = %.2f, p = %.3f',...
    TBEM2{2,3}, TBEM2{5,3}, TBEM2{2,6}, TBEM2{2,7},TBEM2{3,3}, TBEM2{5,3}, TBEM2{3,6}, TBEM2{3,7}, TBEM2{4,3}, TBEM2{5,3}, TBEM2{4,6}, TBEM2{4,7});
tstr2 = 'Left Eye';
title({tstr2; str});
%}