%% Function: report_embyfadingcond.m
%{


Will provide the timestamps of microsaccades in each trial along with the
most recent report of either total or partial fading. Does this on a
trial-by-trial basis (user-specified).

Emin Alicic, 2019, AP Lab
%}

%% Preprocess data

% Clear and close everything
clear all
close all

% Set the path to data
subject = 'MAC';
data = ('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat');
load('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat')
[em] = fading_CalList(data); %EIS read

%%

for tr = 1:size(data.user,1)
    
    % trigger points
    RT(tr,:) = any(data.stream00{tr}.data == 7);  %right trigger
    
    LT(tr,:) = any(data.stream01{tr}.data == 7);  %left trigger
    
    % RIGHT
    tp_r = find(data.stream00{tr}.data == 7,1);
    
    if isempty(tp_r)
        FadeTM_total(tr,:) = nan;
    else
        FadeTM_total(tr,:) = double(data.stream00{tr}.ts(tp_r));
    end
    
    % LEFT
    tp_l = find(data.stream01{tr}.data == 7,1);
    
    if isempty(tp_l)
        FadeTM_partial(tr,:) = nan;
    else
        FadeTM_partial(tr,:) = double(data.stream01{tr}.ts(tp_l));
    end
    
end

%%
% Ask user to specify what trial

    for i=1:length(data.user)
        prompt = ['Enter a trial no between 1 and ',num2str(length(data.user)),':'];
        %      close all;
        tr = input(prompt);
        if isempty(tr)
            break
        end
        
        % Microsaccade timestamp
        ms_start1 = em{tr,1}.microsaccades.start;
        ms_start2 = em{tr,2}.microsaccades.start;
        
        % Fading timestamp
        totalFading = FadeTM_total(tr,:);
        partialFading = FadeTM_partial(tr,:);
        
        break
    end
    
    cmspf1 = nearestpoint(partialFading,ms_start1);
    cmspf2 = nearestpoint(partialFading,ms_start2);
    if isnan(cmspf1)
        disp('You had no reports of partial fading during this trial.')
    elseif ms_start1(cmspf1) < partialFading
        closestMS_pf1 = nearestpoint(partialFading,ms_start1,'next');
        disp(['You reported partial fading during this trial at ', num2str(partialFading), ' ms.'...,
            ' The nearest right eye microsaccade occurred at ', num2str(ms_start1(closestMS_pf1)),' ms.']);
    elseif ms_start1(cmspf1) >= partialFading
        disp(['You reported partial fading during this trial at ', num2str(partialFading), ' ms.'...,
            ' The nearest right eye microsaccade occurred at ', num2str(ms_start1(cmspf1)),' ms.']);
    end
    
    
    if isnan(cmspf2)
        disp('You had no reports of partial fading during this trial.')
    elseif ms_start2(cmspf2) < partialFading
        closestMS_pf2 = nearestpoint(partialFading,ms_start2,'next');
        disp(['You reported partial fading during this trial at ', num2str(partialFading), ' ms.'...,
            ' The nearest left eye microsaccade occurred at ', num2str(ms_start2(closestMS_pf2)),' ms.']);
    elseif ms_start2(cmspf2) >= partialFading
        disp(['You reported partial fading during this trial at ', num2str(partialFading), ' ms.'...,
            ' The nearest left eye microsaccade occurred at ', num2str(ms_start2(cmspf2)),' ms.']);
    end
    
    
    cmstf1 = nearestpoint(totalFading,ms_start1);
    cmstf2 = nearestpoint(totalFading,ms_start2);
    if isnan(cmstf1)
        disp('You had no reports of total fading during this trial.')
    elseif ms_start1(cmstf1) < totalFading
        closestMS_tf1 = nearestpoint(totalFading,ms_start1,'next');
        disp(['You reported total fading during this trial at ', num2str(totalFading), ' ms.'...,
            ' The nearest right eye microsaccade occurred at ', num2str(ms_start1(closestMS_tf1)),' ms.']);
    elseif ms_start1(cmstf1) >= totalFading
        disp(['You reported total fading during this trial at ', num2str(totalFading), ' ms.'...,
            ' The nearest left eye microsaccade occurred at ', num2str(ms_start1(cmstf1)),' ms.']);
    end
    
    
    if isnan(cmstf2)
        disp('You had no reports of total fading during this trial.')
    elseif ms_start2(cmstf2) < totalFading
        closestMS_tf2 = nearestpoint(totalFading,ms_start2,'next');
        disp(['You reported total fading during this trial at ', num2str(totalFading), ' ms.'...,
            ' The nearest left eye microsaccade occurred at ', num2str(ms_start2(closestMS_tf2)),' ms.']);
    elseif ms_start2(cmstf2) >= totalFading
        disp(['You reported total fading during this trial at ', num2str(totalFading), ' ms.'...,
            ' The nearest left eye microsaccade occurred at ', num2str(ms_start2(cmstf2)),' ms.']);
    end

    
