%Fading

%% load in some data
fld = 'EA';

pathtodata = fullfile(pathToBOX(), 'BinocularFading',...
    'Data', fld);
savepath = fullfile(pathToBOX(), 'BinocularFading', 'Data');

if ~exist(savepath, 'dir')
    mkdir(savepath); 
end


if ~exist(fullfile(pathtodata, 'pptrials.mat'), 'file')
    fprintf(fullfile(pathtodata, 'pptrials.mat'));
end
data = readdata(pathtodata, CalList(), true);


pptrials = preprocessing(data);

save(fullfile(savepath, sprintf('%s_pptrials.mat', fld)), 'data', 'pptrials');
%

