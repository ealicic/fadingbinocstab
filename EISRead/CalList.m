%%CalList.m
%{
This is the function that I use for reading in data from the fading
experiment.
%}

%% EIS Read
function [slec] = CalList(data)

% Creates an empty matrix - 'slec' - and fills it with formatted
% information using EISToolbox
slec = eis_readData_binocular([], 'x1');
slec = eis_readData_binocular(slec, 'y1');
slec = eis_readData_binocular(slec, 'x2');
slec = eis_readData_binocular(slec, 'y2');

% Defines L/R triggers
slec = eis_readData_binocular(slec, 'stream', 0, 'int'); % right trigger
slec = eis_readData_binocular(slec, 'stream', 1, 'int'); % left trigger

% Stabilized eye data
slec = eis_readData_binocular(slec, 'stream', 2, 'double'); % stabilized eye 1 data x1.y1
slec = eis_readData_binocular(slec, 'stream', 3, 'double');  % stabilized eye 2 data x1.y1

% Defines blinks and no tracks

slec = eis_readData_binocular(slec, 'trigger', 'blink1');
slec = eis_readData_binocular(slec, 'trigger', 'notrack1');
slec = eis_readData_binocular(slec, 'trigger', 'blink2');
slec = eis_readData_binocular(slec, 'trigger', 'notrack2');

slec = eis_readData(slec,'photocell');
slec = eis_readData(slec,'spf');

slec = eis_readData(slec, 'uservar','SubjectName'); %good
slec = eis_readData(slec, 'uservar','DEBUG'); %good
slec = eis_readData(slec, 'uservar','Trial'); %good

slec = eis_readData(slec, 'uservar','StabCond'); %good
slec = eis_readData(slec, 'uservar','EyeCond'); %good

slec = eis_readData(slec, 'uservar','RecalFrequency'); %good
slec = eis_readData(slec, 'uservar','RecalIncrement'); %good
slec = eis_readData(slec, 'uservar','RecalOffsetX1'); %good
slec = eis_readData(slec, 'uservar','RecalOffsetY1'); %good
slec = eis_readData(slec, 'uservar','RecalOffsetX2'); %good
slec = eis_readData(slec, 'uservar','RecalOffsetY2'); %good

slec = eis_readData(slec, 'uservar','PixelAngle'); %good


%other new fading-specific variables

%if subject reports loss of fading
slec = eis_readData(slec, 'uservar','Aborted'); %good

slec = eis_readData(slec, 'uservar','Expired'); %good
slec = eis_readData(slec, 'uservar','ReCalPreceded'); %good

slec = eis_readData(slec, 'uservar','evt_23_t0'); %good
slec = eis_readData(slec, 'uservar','evt_23_t1'); %good
slec = eis_readData(slec, 'uservar','evt_23_f0'); %good
slec = eis_readData(slec, 'uservar','evt_23_f1'); %good

slec = eis_readData(slec, 'uservar','evt_24_t0'); %good
slec = eis_readData(slec, 'uservar','evt_24_t1'); %good
slec = eis_readData(slec, 'uservar','evt_24_f0'); %good
slec = eis_readData(slec, 'uservar','evt_24_f1'); %good

slec = eis_readData(slec, 'uservar','NumberStim'); %good
slec = eis_readData(slec, 'uservar','StabRingNum'); %good
slec = eis_readData(slec, 'uservar','MonocularOffset'); %good
slec = eis_readData(slec, 'uservar','Patch'); %good
slec = eis_readData(slec, 'uservar','Ring'); %good
slec = eis_readData(slec, 'uservar','Disparity'); %good

slec = eis_readData(slec, 'uservar','stimEcc0'); %good
slec = eis_readData(slec, 'uservar','stimEcc1'); %good
slec = eis_readData(slec, 'uservar','stimEcc2'); %good
slec = eis_readData(slec, 'uservar','stimWidth0'); %good
slec = eis_readData(slec, 'uservar','stimWidth1'); %good
slec = eis_readData(slec, 'uservar','stimWidth2'); %good
slec = eis_readData(slec, 'uservar','stimEnv0'); %good
slec = eis_readData(slec, 'uservar','stimEnv1'); %good
slec = eis_readData(slec, 'uservar','stimEnv2'); %good
slec = eis_readData(slec, 'uservar','stimTheta0'); %good
slec = eis_readData(slec, 'uservar','stimTheta1'); %good
slec = eis_readData(slec, 'uservar','stimTheta2'); %good
slec = eis_readData(slec, 'uservar','stimColor0R'); %good
slec = eis_readData(slec, 'uservar','stimColor0G'); %good
slec = eis_readData(slec, 'uservar','stimColor0B'); %good
slec = eis_readData(slec, 'uservar','stimColor1R'); %good
slec = eis_readData(slec, 'uservar','stimColor1G'); %good
slec = eis_readData(slec, 'uservar','stimColor1B'); %good
slec = eis_readData(slec, 'uservar','stimColor2R'); %good
slec = eis_readData(slec, 'uservar','stimColor2G'); %good
slec = eis_readData(slec, 'uservar','stimColor2B'); %good

slec = eis_readData(slec, 'uservar','StimulusDuration'); %good

slec = eis_readData(slec, 'uservar','RightOriginX'); %good
slec = eis_readData(slec, 'uservar','RightOriginY'); %good
slec = eis_readData(slec, 'uservar','LeftOriginX'); %good
slec = eis_readData(slec, 'uservar','LeftOriginY'); %good

slec = eis_readData(slec, 'uservar','FIXGATE'); %good
slec = eis_readData(slec, 'uservar','FixRadius'); %good
slec = eis_readData(slec, 'uservar','ExperimentName'); %good
slec = eis_readData(slec, 'uservar','SlowStab'); %good


slec = eis_readData(slec, 'uservar','ViewDistance'); %good
slec = eis_readData(slec, 'uservar','HorizontalSize'); %good
slec = eis_readData(slec, 'uservar','VerticalSize'); %good
slec = eis_readData(slec, 'uservar','HorizontalResolution'); %good
slec = eis_readData(slec, 'uservar','VerticalResolution'); %good
slec = eis_readData(slec, 'uservar','RefreshRate'); %good
slec = eis_readData(slec, 'uservar','StereoStep'); %good
slec = eis_readData(slec, 'uservar','Aperture_Radius'); %good
slec = eis_readData(slec, 'uservar','Aperture_Width'); %good
slec = eis_readData(slec, 'uservar','FixCue'); %good
slec = eis_readData(slec, 'uservar','FixSize'); %good
slec = eis_readData(slec, 'uservar','FixWidth'); %good
slec = eis_readData(slec, 'uservar','Crosshair_Color'); %good
slec = eis_readData(slec, 'uservar','Crosshair_Width'); %good
slec = eis_readData(slec, 'uservar','Crosshair_Radius1'); %good
slec = eis_readData(slec, 'uservar','Crosshair_Radius2'); %good
slec = eis_readData(slec, 'uservar','Crosshair_Radius3'); %good
slec = eis_readData(slec, 'uservar','Crosshair_Radiusf'); %good
slec = eis_readData(slec, 'uservar','Crosshair_Drawr1'); %good

%% Create vectors where information about vars is stored

info.StabCond = {'Stabilized','Right Eye Stabilized','Left Eye Stabilized','Normal','Version Stabilized','Vergence Stabilized','Horiztonal Stabilized','Vertical Stabilized'};
info.EyeCond   = {'Binocular','RightEye','LeftEye'};
info.RightStabTrace = 'stream02'; % x.y
info.LeftStabTrace  = 'stream03'; % x.y
info.StabTraceType  = 'float';
info.RightEyeIdx    = 1;
info.LeftEyeIdx     = 2;
info.codes          = getEventCodes();
end
