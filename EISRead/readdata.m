%fading

function data = readdata(pt, list, dosave, fname)

% LIST is the list of variables to import for 
% the specific experiment.
if ~exist('fname', 'var')
    fname = [pt,'/results.mat'];
end
if ~exist('dosave', 'var')
    dosave = false; 
end

% this converts a whole directory
data = eis_eisdir2mat(pt, list, fname);

if dosave
    save(fname, 'data');
end
end
 