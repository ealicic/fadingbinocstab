% Fading

% This file is a modified version of Preprocess_Data()
% It takes a variable containing a list of the entries
% to implement.
%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Process eye movements data and create a list of trials.
% Only the period of stimulus presentation is examined.
% Eye movements are classified for all trials. No selection of trials is
% operated.
%
%  [ValidTrials] = PreprocessTrials(data,NumberOfTrials,MinSaccSpeed, MinSaccAmp,...
%                            MinMSaccSpeed, MinMSaccAmp, MaxMSaccAmp,RefractoryPeriod)


function [ValidTrials] = preprocessing(data)
%     data,NumberOfTrials,MinSaccSpeed, MinSaccAmp,...
%     MinMSaccSpeed, MinMSaccAmp, MaxMSaccAmp,MinVelocity);

% THESE PARAMS SHOULD BE DEFAULT TO BE OVERWRITTEN ONLY
% WHEN A NEW SET OF PARAMS IS SPECIFIED

MinSaccSpeed = 180;
MinSaccAmp = 30;
MinMSaccSpeed = 180;
MinMSaccAmp = 3;
MaxMSaccAmp = 30;
MinVelocity = 120;
NumberOfTrials = length(data.x1);

fprintf('\t Preprocessing trials\n');

ValidTrials = {};
ValidTrialsCounter = 0;

for TrialNumber = 1:NumberOfTrials
    DataValid = [];
    % Get the data relative to the trial
    
    % JI: what are x and y now that this is binocular?
    X1 = (data.x1{TrialNumber});
    Y1 = (data.y1{TrialNumber});
    X2 = (data.x2{TrialNumber});
    Y2 = (data.y2{TrialNumber});
    
    Blink1 = (data.triggers{TrialNumber}.blink1);
    NoTrack1 = (data.triggers{TrialNumber}.notrack1);
    DataValid1 = ones(1,length(data.x1{TrialNumber}));
    
    Trial1 = createTrial(TrialNumber,...
        X1, Y1, Blink1, NoTrack1, DataValid1);
    
    Trial1 = preprocessSignals(Trial1, 'minvel',MinVelocity, 'noanalysis');
    %%%% 'noanalysis' keeps the saccades at the very onset of the trial
    
    
    % Find all valid saccades with speed greater than 3 deg/sec
    % and bigger than 30 arcmin
    Trial1 = findSaccades(Trial1, 'minvel',MinSaccSpeed, 'minsa',MinSaccAmp);
    
    % Find all valid microsaccades with speed greater than 3 deg/sec
    % and amplitude included in 3 arcmin and 60 arcmin
    Trial1 = findMicrosaccades(Trial1, 'minvel',MinMSaccSpeed,'minmsa', MinMSaccAmp,'maxmsa', MaxMSaccAmp);
    Trial1 = findDrifts(Trial1);
    
    %%%%% now do the second eye
    Blink2 = (data.triggers{TrialNumber}.blink2);
    NoTrack2 = (data.triggers{TrialNumber}.notrack2);
    DataValid2 = ones(1,length(data.x2{TrialNumber}));
    
    Trial2 = createTrial(TrialNumber,...
        X2, Y2, Blink2, NoTrack2, DataValid2);
    
    Trial2 = preprocessSignals(Trial2, 'minvel',MinVelocity, 'noanalysis');
    %%%% 'noanalysis' keeps the saccades at the very onset of the trial
    
    
    % Find all valid saccades with speed greater than 3 deg/sec
    % and bigger than 30 arcmin
    Trial2 = findSaccades(Trial2, 'minvel',MinSaccSpeed, 'minsa',MinSaccAmp);
    
    % Find all valid microsaccades with speed greater than 3 deg/sec
    % and amplitude included in 3 arcmin and 60 arcmin
    Trial2 = findMicrosaccades(Trial2, 'minvel',MinMSaccSpeed,'minmsa', MinMSaccAmp,'maxmsa', MaxMSaccAmp);
    Trial2 = findDrifts(Trial2);
    
    % JI: modified for testing - don't extract user data if there isn't any
    if isfield(data, 'user')
        fn=fieldnames(data.user{TrialNumber});
        for ff=1:size(fn,1)
            entry = char(fn(ff));
            Trial1.(entry) = data.user{TrialNumber}.(entry);
            Trial2.(entry) = data.user{TrialNumber}.(entry);
        end
    else
        keyboard;
    end
    
    if ~isempty(data.stream00{TrialNumber}.data)
        Xpos = data.stream00{TrialNumber}.data; %data
        Ypos = data.stream01{TrialNumber}.data;
        XT = data.stream00{TrialNumber}.ts; %times
        YT = data.stream01{TrialNumber}.ts;
        D = double(XT(end))-length(X1);
        if XT(1)==XT(2)
            XT = XT(2:end);
            YT = YT(2:end);
            Xpos = Xpos(2:end);
            Ypos = Ypos(2:end);
        end
        [XTu, ia] = unique(XT);
        Xposu = Xpos(ia);
        [YTu, ia] = unique(YT);
        Yposu = Ypos(ia);
        
        % here add five ms at the beginning
        XStab = eis_expandVector(Xposu, XTu, length(X1), 'replica');
        YStab = eis_expandVector(Yposu, YTu, length(X1), 'replica');
    else
        XStab = [];
        YStab = [];
    end
    Trial1.XStab = XStab * Trial1.PixelAngle;
    Trial1.YStab = YStab * Trial1.PixelAngle;
    
    
    
    
    
    if ~isempty(data.stream02{TrialNumber}.data)
        Xpos = data.stream02{TrialNumber}.data; %data
        Ypos = data.stream03{TrialNumber}.data;
        XT = data.stream02{TrialNumber}.ts; %times
        YT = data.stream03{TrialNumber}.ts;
        D = double(XT(end))-length(X2);
        if XT(1)==XT(2)
            XT = XT(2:end);
            YT = YT(2:end);
            Xpos = Xpos(2:end);
            Ypos = Ypos(2:end);
        end
        [XTu, ia] = unique(XT);
        Xposu = Xpos(ia);
        [YTu, ia] = unique(YT);
        Yposu = Ypos(ia);
        % here add five ms at the beginning
        XStab = eis_expandVector(Xposu, XTu, length(X2), 'replica');
        YStab = eis_expandVector(Yposu, YTu, length(X2), 'replica');
    else
        XStab = [];
        YStab = [];
    end
    Trial2.XStab = XStab * Trial2.PixelAngle;
    Trial2.YStab = YStab * Trial2.PixelAngle;
    
    
    
    if ~isempty(Trial1) && ~isempty(Trial2)
        ValidTrials(ValidTrialsCounter+1, 1) = {Trial1};
        ValidTrials(ValidTrialsCounter+1, 2) = {Trial2};
        ValidTrialsCounter = ValidTrialsCounter + 1;
    end
    
end
