% This function creates an initialized trial structure from the provided
% experimental data.
%
% Syntax:
%   Result = createTrial(ID, xMovement, yMovement)
%   Result = createTrial(ID, xMovement, yMovement, Blinks)
%   Result = createTrial(ID, xMovement, yMovement, Blinks, NoTracks)
%   Result = createTrial(ID, xMovement, yMovement, ...
%     Blinks, NoTracks, Analysis)
%   Result = createTrial(ID, xMovement, yMovement, ...
%     Blinks, NoTracks, Analysis, Invalid)
%
% ID             : Trial identification (number or string)
% xMovement      : Vector of the X eye positions (in arcmin)
% yMovement      : Vector of the Y eye positions (in arcmin)
% Blinks         : Vector of the blink events (default: none)
% NoTracks       : Vector of the no-track events (default: none)
% Analysis       : Vector of the valid analysis period of the trial 
%                  (default: all)
% Invalid        : Vector of the invalid periods of the trial 
%                  (default: none)
%
% Result         : Structure containing information about the trial
%   id           : Trial identification (number or string)
%   samples      : Number of samples of the trial
%   x.position   : X Eye movement data (in arcmin)
%   x.velocity   : X Eye movement velocity (in arcmin/s)
%   y.position   : Y Eye movement data (in arcmin)
%   y.velocity   : Y Eye movement velocity (in arcmin/s)
%   velocity     : Velocity amplitude (in arcmin/s)
%   blinks       : Blink events
%   notracks     : No-track events
%   analysis     : Periods of data analysis
%   saccades     : Event list of the valid saccades (default: empty)
%   microsaccades: Event list of the valid microsaccades (default: empty)
%   drifts       : Event list of the valid drifts (default: empty)
%   fixations    : Event list of the valid fixations (default: empty)
%   invalid      : Event list of the invalid periods (default: empty)
%
function Result = createTrialB(ID, xMovement1, yMovement1, ...
    xMovement2, yMovement2,...
    Blinks, NoTracks, Analysis, Invalid)

  % Initialize by default the missing parameters
  if nargin < 6, Blinks = zeros(size(xMovement1)); end
  if nargin < 7, NoTracks = zeros(size(xMovement1)); end
  if nargin < 8, Analysis = ones(size(xMovement1)); end
  if nargin < 9, Invalid = zeros(size(xMovement1)); end

  % Initialize the general information
  Result.id = ID;

  % Calculate the velocity of the
  % components and the 
  if isempty(xMovement1) || isempty(yMovement1) ||...
          isempty(xMovement2) || isempty(yMovement2)
    xVelocity1 = [];
    yVelocity1 = [];
    xVelocity2 = [];
    yVelocity2 = [];
  else
    xVelocity1 = p_calculateVelocity(xMovement1, 'prefilter', true, ...
      'postfilter', true, 'postvalue', true, 'smoothing', 51);
    yVelocity1 = p_calculateVelocity(yMovement1, 'prefilter', true, ...
      'postfilter', true, 'postvalue', true, 'smoothing', 51);
    xVelocity2 = p_calculateVelocity(xMovement1, 'prefilter', true, ...
      'postfilter', true, 'postvalue', true, 'smoothing', 51);
    yVelocity2 = p_calculateVelocity(yMovement1, 'prefilter', true, ...
      'postfilter', true, 'postvalue', true, 'smoothing', 51);
  end

  % Calculate the amplitude of the velocity vector
  Velocity1 = sqrt(xVelocity1 .^ 2 + yVelocity1 .^ 2) * 1000;
  Velocity2 = sqrt(xVelocity2 .^ 2 + yVelocity2 .^ 2) * 1000;

  % Store the total velocity
  Result.velocity1 = Velocity1;
  Result.velocity2 = Velocity2;

  % Store all information relative the X component
  % of the eye traces
  Result.x1.position = xMovement1;
  Result.x1.velocity = xVelocity1;
  Result.x2.position = xMovement2;
  Result.x2.velocity = xVelocity2;

  % Store all information relative the Y component
  % of the eye traces
  Result.y1.position = yMovement1;
  Result.y1.velocity = yVelocity1;
  Result.y2.position = yMovement2;
  Result.y2.velocity = yVelocity2;

  % Trasform in events and store all the information
  % regarding blinks, no-track, and analysis events
  Result.blinks = vector2events(Blinks);
  Result.notracks = vector2events(NoTracks);

  % Intersect blinks and no-track to avoid that
  % the samples belong to two different type of invalid events 
  Result.blinks = sdiffEvents(1, Result.blinks, Result.notracks);
  Result.notracks = sdiffEvents(2, Result.blinks, Result.notracks);

  Result.analysis = vector2events(Analysis);
  Result.invalid = vector2events(Invalid);

  Result.saccades = createEvents();
  Result.microsaccades = createEvents();
  Result.drifts = createEvents();
  Result.fixations = createEvents();

  % Initialize the number of samples of the Result
  Result.samples = length(Result.x1.position);

  Result = orderfields(Result);
end