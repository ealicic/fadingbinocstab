%%
%{

plot_percentfading.m


This function will plot bar graphs that shows the percent of total or
partial fading based upon the different stabilization conditions.

Note - should be updated soon to reflect new stabilization conditions.


Emin Alicic, AP Lab, 2019.


%}

%% Preprocess data

% Clear and close everything
%clear all
%close all

% Set the path to data
%subject = 'EA';
%data = ('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat');
%load('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat')
%[em] = fading_CalList(data); %EIS read

clear all
subject = 'temp';
path = ('Users/EminAlicic/Desktop/AP Lab Files/Stereo Fading/Data');
pathtodata = fullfile(path, subject);

read_Data = 0; %if set to 1 all data is read, if 0 previous mat file is loaded
if (read_Data == 1)
    [data, files] = fading_readdata(pathtodata,fading_CalList(),true); %set to true to save in mat file and then use load.
else
    load(fullfile(pathtodata, 'results.mat'));
end

em = preprocessBinocularEM(data);


%%
% Creates matrices for stabilization, right trigger, left trigger, and
% fading condition

EYEC = nan(1,size(data.user,1));
STABC = nan(1,size(data.user,1));
ALL_COND = nan(1,size(data.user,1));
NSWCH = nan(1,size(data.user,1));
allem = nan(1,size(data.user,1));

for tr = 1:size(data.user,1)
    
    COND(tr,:) = [data.user{tr}.EyeCond data.user{tr}.StabCond];
    
    RT(tr,:) = any(data.stream00{tr}.data == 7); 
    LT(tr,:) = any(data.stream01{tr}.data == 7);
    
    FadeTM(tr,:) = data.stream00{tr}.ts(find(data.stream00{tr}.data == 7,1)); 
    
%    retrieve my data
    eye_d = double(data.user{tr}.EyeCond);
    stab_d = double(data.user{tr}.StabCond);
    ms_d = em{tr,1}.microsaccades.start;
    
    %store my data
    EYEC(tr) = eye_d;
    STABC(tr) = stab_d;
    COND(tr,:) = [data.user{tr}.EyeCond data.user{tr}.StabCond];
    
    %separate data for the box plot
    if eye_d == 0 && stab_d == 0
        ALL_COND(tr) = 2;
    elseif eye_d == 0 && stab_d == 3
        ALL_COND(tr) = 1;
    elseif eye_d == 1 && stab_d == 0
        ALL_COND(tr) = 4;
    elseif eye_d == 1 && stab_d == 3
        ALL_COND(tr) = 3;
    elseif eye_d == 2 && stab_d == 0
        ALL_COND(tr) = 6;
    elseif eye_d == 2 && stab_d == 3
        ALL_COND(tr) = 5;
    else
        ALL_COND(tr) = nan;
    end
    
    %binning all stab and norm conditions together
    if eye_d == 0 && stab_d == 3
        all_norm(tr) = 1;
    elseif eye_d == 1 && stab_d == 3
        all_norm(tr) = 1;
    elseif eye_d == 2 && stab_d == 3
        all_norm(tr) = 1;
    else
        all_norm(tr) = nan;
    end
    
    if eye_d == 0 && stab_d == 0
        all_stab(tr) = 2;
    elseif eye_d == 1 && stab_d == 0
        all_stab(tr) = 2;
    elseif eye_d == 2 && stab_d == 0
        all_stab(tr) = 2;
    else
        all_stab(tr) = nan;
    end
end
%% Creating the figures

figure('units','in','position',[0 0 1 3/4 ] *7)
clf

% Setting conditions for certain fading conditions, and depending on those
% conditions, allocating data to given matrices and creating axis labels
% for the plot.

for fading_cond = 1:2 % changed 'p' to 'fading_cond' to make it clear that it's referring to the fading condition (partial or total)

    if fading_cond == 1
ystr =  'Percent Total Fading'; 
DATA = RT; 

    elseif fading_cond == 2
    ystr =  'Percent Partial Fading'; 
DATA = LT; 
    end

% defines the two eyes, and defines conditions for normal or stabilization
eyes = [1 2]; clear u 
for eye = 0:2
    
    if eye ~= 0 % I *think* this is for left- or right-eye-only conditions 
    
        normal =  COND(:,1) == eye & ...
           (COND(:,2) == 3 | COND(:,2) == eyes(eyes~=eye));
       
        stab = COND(:,1) == eye & ...
           (COND(:,2) == eye | COND(:,2) == 0);
      
       u(eye+1,1) =  mean(DATA(normal)); 
       u(eye+1,2) =  mean(DATA(stab)); 
    
       n(eye+1,1) =  numel(DATA(normal)); % is 'n' supposed to refer to anything special?
       n(eye+1,2) =  numel(DATA(stab)); 
       
    else % I *think* this is for binocular conditions
        
         normal =  COND(:,1) == eye & ...
           (COND(:,2) == 3) ;
       
        stab = COND(:,1) == eye & ...
           ( COND(:,2) == 0);
      
       u(eye+1,1) =  mean(DATA(normal));
       u(eye+1,2) =  mean(DATA(stab)); 
       
       n(eye+1,1) =  numel(DATA(normal));
       n(eye+1,2) =  numel(DATA(stab)); 
    end
end

% Customizing the plots
%{
subplot(1,2,fading_cond)
bar(u*100); hold on
 set(gca,'box','off','tickdir','out');
 set(gca,'XtickLabel',{'Binocular','RightEye','LeftEye'})
 legend('Normal','Stabilized','Location','best')
 ylabel(ystr); 
 ylim([0 100])
 for eye = 1:3
     for stab = 1:2
 text(eye + -0.2 + (stab-1)/4,0, sprintf('%u',n(eye,stab)),...
     'VerticalAlignment','bottom')
     end
 end
 text(min(xlim),5,'Trial N =','VerticalAlignment','bottom','HorizontalAlignment','Left')
 title(sprintf('MAC Fading Report\n%s',ystr(9:end)))
 %}
 
end

%partialFading_norm = mean
%plot_partialFading = [1, LT];

avg_PN = nanmean(all_norm(LT)/length(LT));%average trials in which partial fading occurred, no stab
avg_PS = nanmean(all_stab(LT)/length(LT)); %avg trials partial fading; stab

avg_TN = nanmean(all_norm(RT)/length(RT));
avg_TS = nanmean(all_stab(RT)/length(RT));

stabplot_partial = [1, avg_PS];
normplot_partial = [1, avg_PN];
    
stabplot_total = [1, avg_TS];
normplot_total = [1, avg_TN];    

plot([normplot_partial(1) stabplot_partial(1)],[normplot_partial(2) stabplot_partial(2)]); hold on;
plot([normplot_total(1) stabplot_total(1)], [stabplot_total(2) stabplot_total(2)]);
%ylim([0 100]);
    
