%% plot_ampang.m
%{
This code is adapted from JI. Can use it to create plots showing the
amplitude and angles of eye movements.

Emin Alicic, 2019, AP Lab
%}

%% Preprocess data

% Clear and close everything
clear all
close all

% Set the path to data
subject = 'MAC';
data = ('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat');
load('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat')
[em] = fading_CalList(data); %EIS read


%% Make the figures
tr = length(data.user);

msamps_1 = em{tr,1}.microsaccades.amplitude;
msangs_1 = em{tr,1}.microsaccades.angle;
msamps_2 = em{tr,2}.microsaccades.amplitude;
msangs_2 = em{tr,2}.microsaccades.angle;

figure(); clf;
subplot(1,5,1);
histogram(msamps_1, 30);
xlabel('Right eye microsaccade amplitude (arcmin)');
ylabel('Count');

subplot(1,5,2);
histogram(msamps_2, 30);
xlabel('Left eye microsaccade amplitude (arcmin)');
ylabel('Count');

subplot(1,5,3);
polarhistogram(msangs_1,30);
title('Microsaccade directions, right eye');

subplot(1,5,4);
polarhistogram(msangs_2,30);
title('Microsaccade directions, left eye');