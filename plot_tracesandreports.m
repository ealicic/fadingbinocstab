%% plot_tracesandreports.m
%{
This function allows you to plot the XY traces for both eyes across all
trials, as well as the trigger reports across all trials.

Emin Alicic, 2019, AP Lab
%}

%% Preprocess data

% Clear and close everything
%clear all
close all

% Set the path to data
clear all
subject = 'MAC';
pathtodata = fullfile(pathToBOX(),'BinocularFading','Data',subject);
pathtodata = fullfile(path, subject);

read_Data = 1; %if set to 1 all data is read, if 0 previous mat file is loaded
if (read_Data == 1)
    [data, files] = readdata(pathtodata,CalList(),true); %set to true to save in mat file and then use load.
else
    load(fullfile(pathtodata, 'results.mat'));
end

em = preprocessBinocularEM(data);
%% Create matrices for data based on conditions

EYEC = nan(1,size(data.user,1));
STABC = nan(1,size(data.user,1));

for tr = 1:size(data.user,1)
    
    %retrieve my data
    eye_d = double(data.user{tr}.EyeCond);
    stab_d = double(data.user{tr}.StabCond);
    ms_d = em{tr,1}.microsaccades.start;
    
    %store my data
    EYEC(tr) = eye_d;
    STABC(tr) = stab_d;
    COND(tr,:) = [data.user{tr}.EyeCond data.user{tr}.StabCond];
    
    if eye_d == 0 && stab_d == 0
        ALL_COND(tr) = 2;
    elseif eye_d == 0 && stab_d == 3
        ALL_COND(tr) = 1;
    elseif eye_d == 1 && stab_d == 0
        ALL_COND(tr) = 4;
    elseif eye_d == 1 && stab_d == 3
        ALL_COND(tr) = 3;
    elseif eye_d == 2 && stab_d == 0
        ALL_COND(tr) = 6;
    elseif eye_d == 2 && stab_d == 3
        ALL_COND(tr) = 5;
    elseif eye_d == 0 && stab_d == 1
        ALL_COND(tr) = 7;
    elseif eye_d == 0 && stab_d == 2
        ALL_COND(tr) = 8;
    elseif eye_d == 1 && stab_d == 1
        ALL_COND(tr) = 9;
    elseif eye_d == 1 && stab_d == 2
        ALL_COND(tr) = 10;
    elseif eye_d == 2 && stab_d == 2
        ALL_COND(tr) = 12;
    elseif eye_d == 2 && stab_d == 1
        ALL_COND(tr) = 11;
    else
        ALL_COND(tr) = nan;
    end
    
    % trigger points
    RT(tr,:) = any(data.stream00{tr}.data == 7);  %right trigger
    
    LT(tr,:) = any(data.stream01{tr}.data == 7);  %left trigger
    
    % RIGHT
    tp_r = find(data.stream00{tr}.data == 7,1);
    
    if isempty(tp_r)
        FadeTM_total(tr,:) = nan;
    else
        FadeTM_total(tr,:) = double(data.stream00{tr}.ts(tp_r));
    end
    
    % LEFT
    tp_l = find(data.stream01{tr}.data == 7,1);
    
    if isempty(tp_l)
        FadeTM_partial(tr,:) = nan;
    else
        FadeTM_partial(tr,:) = double(data.stream01{tr}.ts(tp_l));
    end
    
end

%% Set title strings, allocate data based on condition
for fading_cond = 1:2 % changed 'p' to 'fading_cond' to make it clear that it's referring to the fading condition (partial or total)
    
    if fading_cond == 1
        tstr =  'Total Fading';
        ystr = 'Time from Stimulus Onset to First Report of Total Fading (ms)';
        DATA = FadeTM_total;
        
    elseif fading_cond == 2
        tstr = 'Partial Fading';
        ystr =  'Time from Stimulus Onset to First Report of Partial Fading (ms)';
        DATA = FadeTM_partial;
    end
end

%% Eye movement analysis - how do microsaccades and drifts look when the stimuli is binocularly stabilized?

Fs = 1000; %the data was recorded on the DPI which is at 1000 Hz
yl = ylim;

%% DEFINE A TRIAL

for i=1:length(data.user)
    prompt = ['Enter a trial no between 1 and ',num2str(length(data.user)),' ,Input - '];
    %      close all;
    tr = input(prompt);
    if isempty(tr)
        break
    end
    
    %% Filtering eye movements
    
    % Create a logical vector in which we assume that no blinks and tracks are occuring
    % (false).
    
    isblink1 = false(em{tr,1}.samples,1);
    isblink2 = false(em{tr,2}.samples,1);
    isnotrack1 = false(em{tr,1}.samples,1);
    isnotrack2 = false(em{tr,2}.samples,1);
    
    % loop through each blink and no track, mark its times as true
    for di = 1:length(em{tr}.blinks.start)
        
        startIdx1 = em{tr,1}.blinks.start;
        stopIdx1 = startIdx1 + em{tr,1}.blinks.duration - 1;
        
        startIdx2 = em{tr,2}.blinks.start;
        stopIdx2 = startIdx2 + em{tr,2}.blinks.duration - 1;
        
        isblink1(startIdx1:stopIdx1) = true;
        isblink2(startIdx2:stopIdx2) = true;
    end
    % loop through each no track, mark its times as true
    for nti = 1:length(em{tr}.notracks.start)
        
        nt_startIdx1 = em{tr,1}.notracks.start(nti);
        nt_stopIdx1 = em{tr,1}.notracks.duration(nti) - 1;
        
        nt_startIdx2 = em{tr,2}.notracks.start(nti);
        nt_stopIdx2 = em{tr,2}.notracks.duration(nti) - 1;
        
        isnotrack1(nt_startIdx1:nt_stopIdx1) = true;
        isnotrack2(nt_startIdx2:nt_stopIdx2) = true;
    end
    
    % Define saccade start/stop times for both eyes
    saccadeStartR = em{tr,1}.saccades.start;
    saccadeEndR = saccadeStartR + em{tr,1}.saccades.duration;
    
    saccadeStartL = em{tr,2}.saccades.start;
    saccadeEndL = saccadeStartL + em{tr,2}.saccades.duration;
    
    % Define msaccade start/stop times for both eyes
    msaccadeStartR = em{tr,1}.microsaccades.start;
    msaccadeEndR = msaccadeStartR + em{tr,1}.microsaccades.duration;
    
    msaccadeStartL = em{tr,2}.microsaccades.start;
    msaccadeEndL = msaccadeStartL + em{tr,2}.microsaccades.duration;
    
    %
    tms = (1:em{tr}.samples) / Fs * 1000; % time in ms
    
    % Original x and y traces for both eyes
    xOld_1 = em{tr,1}.x.position;
    yOld_1 = em{tr,1}.y.position;
    xOld_2 = em{tr,2}.x.position;
    yOld_2 = em{tr,2}.y.position;
    
    % Filter out no tracks and blinks for both eyes
    em{tr,1}.x.position(isblink1) = nan;
    em{tr,1}.x.position(isnotrack1) = nan;
    em{tr,2}.x.position(isblink2) = nan;
    em{tr,2}.x.position(isnotrack2) = nan;
    
    em{tr,1}.y.position(isblink1) = nan;
    em{tr,1}.y.position(isnotrack1) = nan;
    em{tr,2}.y.position(isblink2) = nan;
    em{tr,2}.y.position(isnotrack2) = nan;
    
    %Remove overshoots in trials for both eyes
    trial_1 = osRemoverInEM(em{tr,1});
    trial_2 = osRemoverInEM(em{tr,2});
    
    % Overshoot-removed x and y traces for both eyes
    xNew_1 = trial_1.x.position;
    yNew_1 = trial_1.y.position;
    xNew_2 = trial_2.x.position;
    yNew_2 = trial_2.y.position;
    
    
    %% Trigger analysis
    Rtrig = double(data.stream00{tr,1}.data);
    Rtrig_ts = data.stream00{tr,1}.ts;
    Rtrignew = zeros(size(Rtrig));
    Rtrig(Rtrig == 1) = 0;
    Rtrig(Rtrig == 4) = 0;
    Rtrig(Rtrig == 116) = 0;
    Rtrig(Rtrig == 2) = 0;
    Rtrig(Rtrig == 7) = 1;
    
    Ltrig = double(data.stream01{tr,1}.data);
    Ltrig_ts = data.stream01{tr,1}.ts;
    Ltrignew = zeros(size(Ltrig));
    Ltrig(Ltrig == 1) = 0;
    Ltrig(Ltrig == 4) = 0;
    Ltrig(Ltrig == 116) = 0;
    Ltrig(Ltrig == 2) = 0;
    Ltrig(Ltrig == 7) = 1;
    %% Defining the condition
    
    current_cond = ALL_COND(tr);
    if current_cond == 1
        tstr = ' || Condition: Binocular presentation; No stabilization';
    elseif current_cond == 2
        tstr = ' || Condition: Binocular presentation; Binocular stabilization';
    elseif current_cond == 3
        tstr = ' || Condition: Right eye presentation; No stabilization';
    elseif current_cond == 4
        tstr = ' || Condition: Right eye presentation; Binocular stabilization';
    elseif current_cond == 5
        tstr = ' || Condition: Left eye presentation; No stabilization';
    elseif current_cond == 6
        tstr = ' || Condition: Left eye presentation; Binocular stabilization';
    elseif current_cond == 7
        tstr = ' || Condition: Binocular presentation; Stabilized in right eye';
    elseif current_cond == 8
        tstr = ' || Condition: Binocular presentation; Stabilized in left eye';
    elseif current_cond == 9
        tstr = ' || Condition: Right eye presentation; Stabilized in right eye';
    elseif current_cond == 10
        tstr = ' || Condition: Right eye presentation; Stabilized in left eye';
    elseif current_cond == 11
        tstr = ' || Condition: Left eye presentation; Stabilized in right eye';
    elseif current_cond == 12
        tstr = ' || Condition: Left eye presentation; Stabilized in left eye';
    else
        err('ERROR: CONDITION WAS NOT ASSIGNED PROPERLY!!');
    end
    
    %% Plotting x and y DPI traces (binocular)
    % x positions
    figure();
    ax1 = subplot(5,1,1);
    plot(xNew_1); hold on;
    plot(yNew_1);
    xlabel('Time (ms)');
    ylabel('Position (arcmin)');
    title(['E1 DPI Traces. Subject: ' subject, ' Trial: ' num2str(tr), tstr])
    xlim([0 6100]);
    set(gca,'TickDir','Out');
    
    
    %{
    if ~isempty(saccadeStartR)
        vline(saccadeStartR,'g'); hold on;
        vline(saccadeEndR,'r');
        disp(['Right eye saccade(s) in this trial began at ', num2str(saccadeStartR), ...
            ' ms and ended at ', num2str(saccadeEndR), ' ms']);
    else
        disp('No right eye saccades in this trial.');
    end
    
    if ~isempty(saccadeStartL)
        vline(saccadeStartL,'g'); hold on;
        vline(saccadeEndL,'r');
        disp(['Left eye saccade(s) in this trial began at ', num2str(saccadeStartL), ...
            ' ms and ended at ', num2str(saccadeEndL), ' ms']);
    else
        disp('No left eye saccades in this trial.');
    end
    
    if ~isempty(msaccadeStartR)
        plot(msaccadeStartR,0,'ko','markerfacecolor','g');
        plot(msaccadeEndR,0,'ko','markerfacecolor','r');
        disp(['Right eye microsaccade(s) in this trial began at ', num2str(msaccadeStartR), ...
            ' ms and ended at ', num2str(msaccadeEndR), ' ms']);
    else
        disp('No right eye microsaccades in this trial.');
    end
    
    if ~isempty(msaccadeStartL)
        plot(msaccadeStartL,0,'ko','markerfacecolor','b');
        plot(msaccadeEndL,0,'ko','markerfacecolor','y');
        disp(['Left eye microsaccade(s) in this trial began at ', num2str(msaccadeStartL), ...
            ' ms and ended at ', num2str(msaccadeEndL), ' ms']);
    else
        disp('No left eye microsaccades in this trial.');
    end
    %}
    
    legend('X', 'Y','Location', 'Best');
    
    % y positions
    ax2 = subplot(5,1,2);
    plot(yNew_2); hold on
    plot(xNew_2);
    xlabel('Time (ms)');
    ylabel('Position (arcmin)');
    lgdY = legend('X', 'Y','Location', 'Best');
    title('E2 DPI Traces');
    xlim([0 6200]);
    set(gca,'TickDir','Out');
    
    %{
    if ~isempty(saccadeStartL)
        vline(saccadeStartL,'g'); hold on;
        vline(saccadeEndL,'r');
        disp(['Left eye saccade(s) in this trial began at ', num2str(saccadeStartL), ...
            ' ms and ended at ', num2str(saccadeEndL), ' ms']);
    end
    
    
    if ~isempty(saccadeStartR)
        vline(saccadeStartR,'g'); hold on;
        vline(saccadeEndR,'r');
        disp(['Right eye saccade(s) in this trial began at ', num2str(saccadeStartR), ...
            ' ms and ended at ', num2str(saccadeEndR), ' ms']);
    end
    
    if ~isempty(msaccadeStartR)
        plot(msaccadeStartR,0,'ko','markerfacecolor','g');
        plot(msaccadeEndR,0,'ko','markerfacecolor','r');
    end
    
    if ~isempty(msaccadeStartL)
        plot(msaccadeStartL,0,'ko','markerfacecolor','b');
        plot(msaccadeEndL,0,'ko','markerfacecolor','y');
    end
    %}
    
    
    legend('X', 'Y','Location', 'Best');
    
    %% Plotting the trigger reports
    % Left trigger
    ax3 = subplot(5,1,4);
    stairs(Ltrig_ts,Ltrig), hold on;
    title('Left Trigger Reports');
    xlabel('Time (ms)');
    ylabel('Trigger Reports');
    ylim([0 1.5]);
    xlim([0 6200]);
    set(gca,'TickDir','Out');
    
    
    % Right trigger
    ax4 = subplot(5,1,5);
    stairs(Rtrig_ts,Rtrig); hold on
    title('Report Trigger Reports');
    xlabel('Time (ms)');
    ylabel('Trigger Reports');
    ylim([0 1.5]);
    xlim([0 6200]);
    set(gca,'TickDir','Out');
    %% Vergence and version plots
    ax5 = subplot(5,1,3);
    version = (xNew_1 + xNew_2)/2;
    vergence = (xNew_1 - xNew_2)/2;
    plot(version); hold on;
    plot(vergence)
    lgdV = legend('Version', 'Vergence','Location', 'Best');
    xlabel('Time (ms)');
    ylabel('Position (arcmin)');
    title('Vergence and Version Horizontal Traces');
    xlim([0 6200]);
    set(gca,'TickDir','Out');
    
    %linkaxes([ax1 ax2 ax3 ax4 ax5], 'x')
    
    pause
    close all
    
end