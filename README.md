# stereofading

MATLAB analysis code for fading.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

12/11/19

Check organizedata.m for updated labeling conventions

~
~
~

Below, you will find the labeling conventions I used for each possible condition (OLD)



EYEC  &   STABC    ALL_COND       STIMULI PRESENTED ___; STABILIZED IN ___
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 0                  0               2           Binocular, stabilized in both eyes (B.S.)
 0                  1               7           Binocular, stabilized in right eye
 0                  2               8           Binocular, stabilized in left eye
 0                  3               1           Binocular, no stabilization (normal) (B.N.)

 1                  0               4           Right eye, stabilized in both eyes (R.S.)
 1                  1               9           Right eye, stabilized in right eye
 1                  2              10          Right eye, stabilized in left eye
 1                  3                3           Right eye, no stabilization (normal) (R.N.)

 2                  0                6           Left eye, stabilized in both eyes (L.S.)
 2                  1                1          Left eye, stabilized in right eye
 2                  2                12          Left eye, stabilized in left eye
 2                  3                 5           Left eye, no stabilization (normal) (L.N.)


EYEC and eye_d contain the same labeling convention:
0 --> stimuli was presented binocularly
1 --> stimuli was presented to the right eye only
2 --> stimuli was presented to the left eye only


STABC and stab_d contain the same labeling convention:
0 --> stimuli was stabilized binocularly
1 --> stimuli was stabilized in the right eye only
2 --> stimuli was stabilized in the left eye only
3 --> stimuli was no stabilized, AKA 'normal'

ALL_COND is the variable that contains the information for conditions 1-6, while td contains the information for all of these.