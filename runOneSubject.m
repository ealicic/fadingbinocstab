function runOneSubject(subjects)

addpath(fullfile(pathToBOX(), 'BinocularFading','Code','BinocularEMAnalysis','Analysis'));
addpath(fullfile(pathToBOX(), 'BinocularFading','Code','BinocularEMAnalysis','BinocularEM'));

run('stabilizationIndex.m');

pathtodata = fullfile(pathToBOX(), 'BinocularFading','Data');

ppt_fname = fullfile(pathtodata, sprintf('%s_pptrials.mat', subjects));
ppt_file = dir(ppt_fname);
load(ppt_fname, 'pptrials');

%% remove saccade overshoots
pptrials_orig = pptrials;
pptrials(:, 1) = osRemoverInEM(pptrials(:, 1));
pptrials(:, 2) = osRemoverInEM(pptrials(:, 2));

%% copy pptrials into data structure + add extra fields
flds = {'Disparity', 'PixelAngle', 'Aborted','StimulusDuration'};

clear data;
for fi = 1:length(flds)
    eval(sprintf('data.%s = cellfun(@(x) x.%s, pptrials(:, 1));', flds{fi}, flds{fi}));
end

%% prepare for figures
imgPath = fullfile(pathToBOX(), 'BinocularFading', 'Figures');
subs = {'TrialTraces'};
if ~exist(imgPath, 'dir')
    mkdir(imgPath);
end
for ss = 1:length(subs)
    if ~exist(fullfile(imgPath, subs{ss}), 'dir')
        mkdir(fullfile(imgPath, subs{ss}));
    end
end

%% aparatus and room parameters
dist2monitor = 4235; % JI: I think??????

pixel2angle = pptrials{end, 1}.PixelAngle;

% for now estimate some parameters
p = 60; % interpupillary distance (IPD)
r_L = [0, -p/2, 0]'; % position vectors of eyes in space
r_R = [0, p/2, 0]';
r_mon = [dist2monitor, 0, 0]';
baseline = r_R - r_L;
r_cyc = (r_R + r_L) / 2;

% angular offsets from straight ahead for each
theta_offset(1, 1) = atan2( r_mon(2) - r_R(2), r_mon(1) - r_R(1) );
theta_offset(1, 2) = atan2( r_mon(3) - r_R(3), r_mon(1) - r_R(1) );
theta_offset(2, 1) = atan2( r_mon(2) - r_L(2), r_mon(1) - r_L(1) );
theta_offset(2, 2) = atan2( r_mon(3) - r_L(3), r_mon(1) - r_L(1) );

%% RUN PARAMETERS
MAX_INVALID_DURATION = 400; % ms - limit on blink / no tracks

nanflds = {'blinks', 'notracks'};
emflds = {'drifts', 'microsaccades', 'saccades'};
emlabels = {'d', 'ms', 's'};

params = struct();
params.emfilters = {'driftonly', 'msallowed', 'saccadeallowed',...
    'msonly', 'saccadeonly', 'all'}; % analyze with this filters

% histogram limits / bins for drift parameters
params.nbins = [100, 30]; % instantaneous and average number of bins

params.drift.dur = linspace(100, 1500, 30);
params.drift.pos = linspace(-5, 5, 30);
params.drift.speed_lims = [0, 100];
params.drift.vel_lims = [-100, 100];
params.drift.curv_lims = [0, 80];
params.drift.version_pos = {linspace(-40, 40, 80), linspace(-20, 20, 40)};
params.drift.vergence_pos = {linspace(-40, 40, 80), linspace(-20, 20, 40)};
params.drift.vergence_vel_lims = [-200, 200];
params.drift.vergence_change = {linspace(-40, 40, 80), linspace(-20, 20, 40)};
params.drift.nboots = 100;

params.saccade.amp = linspace(0, 60, 30);
params.saccade.dir = linspace(0, 2*pi, 30);
params.saccade.pkvel = linspace(0, 20, 30);
params.saccade.amp_diff = linspace(-20, 20, 30);
params.saccade.V_amp = linspace(-15, 15, 30);
params.saccade.pkvel_diff = linspace(-30, 30, 30);
params.saccade.S_ampxy = linspace(-80, 80, 30);
params.saccade.S_amp = linspace(0, 80, 30);

%% preload processed/segmented pptrials or compute them here!
fname_seg = sprintf('%s_pptrials_segmented_2.mat', subjects);
fil_seg = dir(fullfile(pathtodata, fname_seg));
if ~exist(fullfile(pathtodata, fname_seg), 'file') || ... % file does not exist
        (exist(fullfile(pathtodata, fname_seg), 'file') && ...
        datenum(fil_seg.date) < datenum(ppt_file.date)) % or segmented file was created before pptrials
    
    %% compute instantaneous eye movements
    % compute vergence, binocular gaze point, velocity, curvature, ... for
    % downstream use
    fname_pr = sprintf('%s_pptrials_processed_2.mat', subjects);
    fil_pr = dir(fullfile(pathtodata, fname_pr));
    if ~exist(fullfile(pathtodata, fname_pr), 'file') || ... % file does not exist
            (exist(fullfile(pathtodata, fname_pr), 'file') &&...
            datenum(fil_pr.date) < datenum(ppt_file.date)) % or processed file was created before pptrials
        
        
        pptrials = processInstantaneousEyeMovements(...
            pptrials, pptrials_orig, pixel2angle, dist2monitor, ...
            baseline, r_cyc, theta_offset);
        
        % undo the skipped calibration procedure in
        % processInstantaneousEyeMovements
        for ii = 1:size(pptrials, 1)
            pptrials{ii, 1}.skippedCalibration = false;
            pptrials{ii, 2}.skippedCalibration = false;
        end
        
        save(fullfile(pathtodata, fname_pr), 'pptrials');
    else
        fprintf('loading %s\n', fname_pr);
        load(fullfile(pathtodata, fname_pr), 'pptrials');
    end
    
    % compute maximum velocitiy in each trace for later
    data.maxVx = nan(size(pptrials, 1), 1);
    for ii = 1:size(pptrials, 1)
        ee = min(pptrials{ii, 1}.samples, floor(pptrials{ii, 1}.timeRespStart));
        useidx = ceil(pptrials{ii, 1}.timeRampStart):ee;
        data.maxVx(ii) = max(abs(pptrials{ii, 1}.vs_components(useidx, 3)/2));
    end
    
    %% trial counts
    counts = countTrials(pptrials);

    pptrials = segmentBinocularEM(pptrials, counts, params);
    save(fullfile(pathtodata, fname_seg), 'pptrials', 'data', 'counts');
else
    fprintf('loading %s\n', 'pptrials_segmented_2.mat');
    load(fullfile(pathtodata, fname_seg), 'pptrials', 'data', 'counts');
end

%% analysis
% for analysis only
pptrials(:, 1) = cellfun(@(x) setfield(x, 'start', 1), pptrials(:, 1), 'UniformOutput', false);

%{
if ~fastversion
    analyzeEyeMovements(subject, data, counts, pptrials, params, imgPath);
    close all;
end

perfData = analyzePerformance(subject, data, counts, params, imgPath);
%}

save(fullfile(pathtodata, sprintf('%s_results.mat', subject)), 'counts', 'data', 'perfData', 'params');
end