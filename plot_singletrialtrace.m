%%
%{
PLOT_SINGLEtrialTRACE.m

The purpose of this code is to produce plots for a single trial. This is
based off on the EA_FADINGANA.m function.
%}

%% Clear workspace, close all figures
% Might want to comment this out depending on what I'm doing!
clear all
close all

%% Load data, set subject
subject = 'MAC';
data = 'Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat';
load(data)
[em] = fading_CalList(data); %EIS read

% Choose a trial to analyze
trialIdx = 2;

%% Separate data based on conditions

EYEC = nan(1,size(data.user,1));
STABC = nan(1,size(data.user,1));
ALL_COND = nan(1,size(data.user,1));
NSWCH = nan(1,size(data.user,1));

for tr = 1:size(data.user,1)
    
    %retrieve my data
    eye_d = double(data.user{tr}.EyeCond);
    stab_d = double(data.user{tr}.StabCond);
    
    %store my data
    EYEC(tr) = eye_d;
    STABC(tr) = stab_d;
    COND(tr,:) = [data.user{tr}.EyeCond data.user{tr}.StabCond];
    
    % Define the right and left triggers
    RT(trialIdx,:) = any(data.stream00{trialIdx}.data == 7); 
    LT(trialIdx,:) = any(data.stream01{trialIdx}.data == 7);
    
    % Find the first time that either trigger was pressed, along with the
    % corresponding time stamps. 
    tp_r = find(data.stream00{trialIdx}.data == 7);
    if isempty(tp_r)
        trigReport_right = nan;
    else
        %trigReport_right = 1;
        trigReport_right(trialIdx) = data.stream00{trialIdx}.ts(data.stream00{trialIdx}.data == 7);
    end
    
    tp_l = find(data.stream01{trialIdx}.data == 7);
    if isempty(tp_l)
        trigReport_left(trialIdx) = nan;
    else
        %trigReport_left(trialIdx) = 1;
        trigReport_left(trialIdx) = data.stream01{trialIdx}.ts(data.stream01{trialIdx}.data==7);
    end
end

%% Filter eye movements

% Removing saccade overshoots

% The original X and Y traces
xOld_1 = em{2,1}.x.position;
xOld_2 = em{2,2}.x.position;
yOld_1 = em{2,1}.y.position;
yOld_2 = em{2,2}.y.position;

% Use JI's function to filter out overshoots
trial_1 = osRemoverInEM(em{2,1});
trial_2 = osRemoverInEM(em{2,2});

% The new X and Y traces
xNew_1 = trial_1.x.position;
xNew_2 = trial_2.x.position;
yNew_1 = trial_1.y.position;
yNew_2 = trial_2.y.position;


% Remove periods of no tracks and blinks

% Creating a logical vector in which we assume that no blinks are occurring
% (false!)
isblink = false(em{trialIdx}.samples);
isnotrack = false(em{trialIdx}.samples);

% Loop through each blink and no track, and mark its times as true
for di = 1:length(em{trialIdx}.blinks.start)
    startIdx = em{trialIdx}.blinks.start(di);
    stopIdx = startIdx + em{trialIdx}.blinks.duration(di) - 1;
    isblink(startIdx:stopIdx) = true;
end

for nti = 1:length(em{trialIdx}.notracks.start)
    nt_startIdx = em{trialIdx}.notracks.start(nti);
    nt_stopIdx = em{trialIdx}.notracks.duration(nti) - 1;
    isnotrack(nt_startIdx:nt_stopIdx) = true;
end

% Removing the values when a blink or no track occurred
xNew_1(isblink, isnotrack) = nan;
xNew_2(isblink, isnotrack) = nan;
yNew_1(isblink, isnotrack) = nan;
yNew_2(isblink, isnotrack) = nan;

%% XY Traces & Fading Reports

% Plotting binocular X and Y DPI traces

righttrig_on = data.stream00{trialIdx}.ts(data.stream01{trialIdx}.data == 7);
righttrig_off = righttrig_on + 100;
lefttrig_on = data.stream01{trialIdx}.ts(data.stream01{trialIdx}.data == 7);
lefttrig_off = lefttrig_on + 100;
figure(); clf;
plot(xNew_1); hold on;
plot(yNew_1);
plot(xNew_2);
plot(yNew_2);

yl = ylim; 
xlabel('Time (ms)');
ylabel('Position (arcmin)');
lgd1 = legend('Right X','Right Y','Left X','Left Y','Location','Best');
lgd1.Title.String = ['Subject: ' subject ', Trial ' num2str(trialIdx)];
title('X and Y DPI Binocular Traces')

  
        





