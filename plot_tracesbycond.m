%% plot_tracesbycond.m
%{
This code goes trial-by-trial and sorts the data based on condition (here,
I used ' td ' instead of ' ALL_COND ', but it's the same thing (side note:
I plan on making those the same at some point). It finds the XY traces for
those conditions, and plots them.

Emin Alicic, 2019, AP Lab
%}

%% Preprocess data

% Clear and close everything
clear all
close all

% Set the path to data
subject = 'MAC';
data = ('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat');
load('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat')
[em] = fading_CalList(data); %EIS read

%% %% Eye movement analysis - how do microsaccades and drifts look when the stimuli is binocularly stabilized?

Fs = 1000; %the data was recorded on the DPI which is at 1000 Hz

%% Filtering eye movements

% Remove saccade overshoots
tr = length(data.user); 

tms = (1:em{tr}.samples) / Fs * 1000; % time in ms


% Removing periods of no tracks and blinks

% Create a logical vector in which we assume that no blinks and tracks are occuring
    % (false).
    
    isblink1 = false(em{tr,1}.samples,1);
    isblink2 = false(em{tr,2}.samples,1);
    isnotrack1 = false(em{tr,1}.samples,1);
    isnotrack2 = false(em{tr,2}.samples,1);
    
    % loop through each blink and no track, mark its times as true
    for di = 1:length(em{tr}.blinks.start)
        
        startIdx1 = em{tr,1}.blinks.start;
        stopIdx1 = startIdx1 + em{tr,1}.blinks.duration - 1;
        
        startIdx2 = em{tr,2}.blinks.start;
        stopIdx2 = startIdx2 + em{tr,2}.blinks.duration - 1;
        
        isblink1(startIdx1:stopIdx1) = true;
        isblink2(startIdx2:stopIdx2) = true;
    end
    % loop through each no track, mark its times as true
    for nti = 1:length(em{tr}.notracks.start)
        
        nt_startIdx1 = em{tr,1}.notracks.start(nti);
        nt_stopIdx1 = em{tr,1}.notracks.duration(nti) - 1;
        
        nt_startIdx2 = em{tr,2}.notracks.start(nti);
        nt_stopIdx2 = em{tr,2}.notracks.duration(nti) - 1;
        
        isnotrack1(nt_startIdx1:nt_stopIdx1) = true;
        isnotrack2(nt_startIdx2:nt_stopIdx2) = true;
    end
    
     % Original x and y traces for both eyes
    xOld_1 = em{tr,1}.x.position;
    yOld_1 = em{tr,1}.y.position;
    xOld_2 = em{tr,2}.x.position;
    yOld_2 = em{tr,2}.y.position;
    
    % Filter out no tracks and blinks for both eyes
    em{tr,1}.x.position(isblink1) = nan;
    em{tr,1}.x.position(isnotrack1) = nan;
    em{tr,2}.x.position(isblink2) = nan;
    em{tr,2}.x.position(isnotrack2) = nan;
    
    em{tr,1}.y.position(isblink1) = nan;
    em{tr,1}.y.position(isnotrack1) = nan;
    em{tr,2}.y.position(isblink2) = nan;
    em{tr,2}.y.position(isnotrack2) = nan;
    
    %Remove overshoots in trials for both eyes
    trial_1 = osRemoverInEM(em{tr,1});
    trial_2 = osRemoverInEM(em{tr,2});
    
    % Overshoot-removed x and y traces for both eyes
    xNew_1 = trial_1.x.position;
    yNew_1 = trial_1.y.position;
    xNew_2 = trial_2.x.position;
    yNew_2 = trial_2.y.position;
    
    
%% XY BINOCULAR DPI TRACES

%% L/R/B Stabilized Traces

td = nan(1,size(data.user,1));

for tr = 1:length(data.user)
    eye_d = double(data.user{tr}.EyeCond);
    stab_d = double(data.user{tr}.StabCond);
    
    if eye_d == 0 && stab_d == 0
        td(tr) = 2;
    elseif eye_d == 0 && stab_d == 3
        td(tr) = 1;
    elseif eye_d == 1 && stab_d == 0
        td(tr) = 4;
    elseif eye_d == 1 && stab_d == 3
        td(tr) = 3;
    elseif eye_d == 2 && stab_d == 0
        td(tr) = 6;
    elseif eye_d == 2 && stab_d == 3
        td(tr) = 5;
    elseif eye_d == 0 && stab_d == 1
        td(tr) = 7;
    elseif eye_d == 0 && stab_d == 2
        td(tr) = 8;
    elseif eye_d == 1 && stab_d == 1
        td(tr) = 9;
    elseif eye_d == 1 && stab_d == 2
        td(tr) = 10;
    elseif eye_d == 2 && stab_d == 1
        td(tr) = 11;
    elseif eye_d == 2 && stab_d == 2
        td(tr) = 12;
    else
        td(tr) = nan;
    end
end

% stimuli presented in left eye, stab in both eyes
for tr = 1:length(data.user)
    lx1pos_stabinoc = xNew_1(td == 6);
    lx2pos_stabinoc = xNew_2(td == 6);
    ly1pos_stabinoc = yNew_1(td == 6);
    ly2pos_stabinoc = yNew_2(td == 6);
end

if td ~= 6
    disp('There were no trials in this data set in which the stimuli was presented in the left eye and stabilized binocularly')
elseif length(lx1pos_stabinoc) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the left eye and stabilized binocularly')
else
    figure(); clf; hold on
    plot(lx1pos_stabinoc)
    plot(lx2pos_stabinoc)
    plot(ly1pos_stabinoc)
    plot(ly2pos_stabinoc)
    td6sum = sum(td(:)==6);
    xlim([1 td6sum]);
    title(['Stimuli presented in left eye, stabilized in both eyes. Trial Count =',num2str(td6sum)])
    xlabel('trial number');
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in left eye, stab in left eye only
for tr = 1:length(data.user)
    lx1pos_stabl = xNew_1(td == 12);
    lx2pos_stabl = xNew_2(td == 12);
    ly1pos_stabl = yNew_1(td == 12);
    ly2pos_stabl = yNew_2(td == 12);
end

if td ~= 12
    disp('There were no trials in this data set in which the stimuli was presented in the left eye and stabilized in the left eye only')
elseif length(lx1pos_stabl) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the left eye and stabilized in the left eye only')
else
    figure(); clf; hold on
    plot(lx1pos_stabl)
    plot(lx2pos_stabl)
    plot(ly1pos_stabl)
    plot(ly2pos_stabl)
    td12sum = sum(td(:) == 12);
    xlim([1 td12sum]);
    title(['Stimuli presented in left eye, stabilized in left eye only. Trial Count =',num2str(td12sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in right eye, stab in both eyes
for tr = 1:length(data.user)
    rx1pos_stabinoc = xNew_1(td == 4);
    rx2pos_stabinoc = xNew_2(td == 4);
    ry1pos_stabinoc = yNew_1(td == 4);
    ry2pos_stabinoc = yNew_2(td == 4);
end

if td ~= 4
    disp('There were no trials in this data set in which the stimuli was presented in the right eye and stabilized binocularly')
elseif length(rx1pos_stabinoc) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the right eye and stabilized binocularly')
else
    figure(); clf; hold on
    plot(rx1pos_stabinoc)
    plot(rx2pos_stabinoc)
    plot(ry1pos_stabinoc)
    plot(ry2pos_stabinoc)
    td4sum = sum(td(:)==4);
    xlim([1 td4sum]);
    title(['Stimuli presented in right eye, stabilized in both eyes. Trial Count =',num2str(td4sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in right eye, stab in right eye only
for tr = 1:length(data.user)
    rx1pos_stabr = xNew_1(td == 9);
    rx2pos_stabr = xNew_2(td == 9);
    ry1pos_stabr = yNew_1(td == 9);
    ry2pos_stabr = yNew_2(td == 9);
end

if td ~= 9
    disp('There were no trials in this data set in which the stimuli was presented in the right eye and stabilized in the right eye only')
elseif length(rx1pos_stabr) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the right eye and stabilized in the right eye only')
else
    figure(); clf; hold on
    plot(rx1pos_stabr)
    plot(rx2pos_stabr)
    plot(ry1pos_stabr)
    plot(ry2pos_stabr)
    td9sum = sum(td(:) == 9);
    xlim([1 td9sum]);
    title(['Stimuli presented in right eye, stabilized in right eye only. Trial Count = ', num2str(td9sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented binocularly, stab binocularly
for tr = 1:length(data.user)
    bx1pos_stabinoc = xNew_1(td == 2);
    bx2pos_stabinoc = xNew_2(td == 2);
    by1pos_stabinoc = yNew_1(td == 2);
    by2pos_stabinoc = yNew_2(td == 2);
end

if td ~= 2
    disp('There were no trials in this data set in which the stimuli was presented binocularly and stabilized binocularly')
elseif length(bx1pos_stabinoc) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented binocularly and stabilized binocularly')
else
    figure(); clf; hold on
    plot(bx1pos_stabinoc)
    plot(bx2pos_stabinoc)
    plot(by1pos_stabinoc)
    plot(by2pos_stabinoc)
    td2sum = sum(td(:)==2);
    xlim([1 td2sum]);
    title(['Stimuli presented binocularly, stabilized binocularly. Trial Count = ',num2str(td2sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented binocularly, stab right eye
for tr = 1:length(data.user)
    bx1pos_stabr = xNew_1(td == 7);
    bx2pos_stabr = xNew_2(td == 7);
    by1pos_stabr = yNew_1(td == 7);
    by2pos_stabr = yNew_2(td == 7);
end

if td ~= 7
    disp('There were no trials in this data set in which the stimuli was presented binocularly and stabilized in the right eye only')
elseif length(bx1pos_stabr) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented binocularly and stabilized in the right eye only')
else
    figure(); clf; hold on
    plot(bx1pos_stabr)
    plot(bx2pos_stabr)
    plot(by1pos_stabr)
    plot(by2pos_stabr)
    td7sum = sum(td(:)==7);
    xlim([1 td7sum]);
    title(['Stimuli presented binocularly, stabilized in right eye only. Trial Count = ', num2str(td7sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in right eye, stab left eye only
for tr = 1:length(data.user)
    rx1pos_stabl = xNew_1(td == 10);
    rx2pos_stabl = xNew_2(td == 10);
    ry1pos_stabl = yNew_1(td == 10);
    ry2pos_stabl = yNew_2(td == 10);
end

if td ~= 10
    disp('There were no trials in this data set in which the stimuli was presented in the right eye and stabilized in the left eye only')
elseif length(rx1pos_stabl) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the right eye and stabilized in the left eye only')
else
    figure(); clf; hold on
    plot(rx1pos_stabl)
    plot(rx2pos_stabl)
    plot(ry1pos_stabl)
    plot(ry2pos_stabl)
    td10sum = sum(td(:) == 10);
    xlim([1 td10sum]);
    title(['Stimuli presented in right eye, stabilized in left eye only. Trial Count = ',num2str(td10sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in left eye, stabilized in right eye
for tr = 1:length(data.user)
    lx1pos_stabr = xNew_1(td == 11);
    lx2pos_stabr = xNew_2(td == 11);
    ly1pos_stabr = yNew_1(td == 11);
    ly2pos_stabr = yNew_2(td == 11);
end

if td ~= 11
    disp('There were no trials in this data set in which the stimuli was presented in the left eye and stabilized in the right eye only')
elseif length(lx1pos_stabr) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the left eye and stabilized in the right eye only')
else
    figure(); clf; hold on
    plot(lx1pos_stabr)
    plot(lx2pos_stabr)
    plot(ly1pos_stabr)
    plot(ly2pos_stabr)
    td11sum = sum(td(:) == 11);
    xlim([1 td11sum]);
    title(['Stimuli presented in left eye, stabilized in right eye only. Trial Count = ',num2str(td11sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented binocularly, stab in left eye only
for tr = 1:length(data.user)
    bx1pos_stabl = xNew_1(td == 8);
    bx2pos_stabl = xNew_2(td == 8);
    by1pos_stabl = yNew_1(td == 8);
    by2pos_stabl = yNew_2(td == 8);
end

if td ~= 8
    disp('There were no trials in this data set in which the stimuli was presented binocularly and stabilized in the left eye only')
elseif length(bx1pos_stabl) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented binocularly and stabilized in the left eye only')
else
    figure(); clf; hold on
    plot(bx1pos_stabl)
    plot(bx2pos_stabl)
    plot(by1pos_stabl)
    plot(by2pos_stabl)
    td8sum = sum(td(:) == 8);
    xlim([1 td8sum]);
    title(['Stimuli presented binocularly, stabilized in the left eye only. Trial Count = ',num2str(ts8sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end
% L/R/B Normal Traces (with msaccades highlighted)

% stimuli presented in left eye, not stab 5
for tr = 1:length(data.user)
    lx1pos_norm = xNew_1(td == 5);
    lx2pos_norm = xNew_2(td == 5);
    ly1pos_norm = yNew_1(td == 5);
    ly2pos_norm = yNew_2(td == 5);
end

if td ~= 5
    disp('There were no trials in this data set in which the stimuli was presented in the right eye and not stabilized')
elseif length(lx1pos_norm) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the right eye and not stabilized')
else
    figure(); clf; hold on
    plot(lx1pos_norm)
    plot(lx2pos_norm)
    plot(ly1pos_norm)
    plot(ly2pos_norm)
    td5sum = sum(td(:) == 5);
    xlim([1 td5sum]);
    title(['Stimuli presented in the right eye, not stabilized. Trial Count = ',num2str(td5sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in right eye, not stab 3
for tr = 1:length(data.user)
    rx1pos_norm = xNew_1(td == 3);
    rx2pos_norm = xNew_2(td == 3);
    ry1pos_norm = yNew_1(td == 3);
    ry2pos_norm = yNew_2(td == 3);
end

if td ~= 3
    disp('There were no trials in this data set in which the stimuli was presented in the left eye and not stabilized')
elseif length(rx1pos_norm) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the left eye and not stabilized')
else
    figure(); clf; hold on
    plot(rx1pos_norm)
    plot(rx2pos_norm)
    plot(ry1pos_norm)
    plot(ry2pos_norm)
    td3sum = sum(td(:) == 3);
    xlim([1 td3sum]);
    title(['Stimuli presented in the left eye, not stabilized. Trial Count = ',num2str(td3sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented binocularly, not stab 1
for tr = 1:length(data.user)
    bx1pos_norm = xNew_1(td == 1);
    bx2pos_norm = xNew_2(td == 1);
    by1pos_norm = yNew_1(td == 1);
    by2pos_norm = yNew_2(td == 1);
end


if td ~= 1
    disp('There were no trials in this data set in which the stimuli was presented binocularly and not stabilized')
elseif length(bx1pos_norm) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented binocularly and not stabilized')
else
    figure(); clf; hold on
    plot(bx1pos_norm)
    plot(bx2pos_norm)
    plot(by1pos_norm)
    plot(by2pos_norm)
    td1sum = sum(td(:) == 1);
    xlim([1 td1sum]);
    title(['Stimuli presented binocularly, not stabilized. Trial Count = ', num2str(td1sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end
