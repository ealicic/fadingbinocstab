%% fadinganalysis_EA.m
%{
** THIS IS MY ORIGINAL SCRIPT WITH ALL CODE IN IT! **



EYEC  &   STABC    ALL_COND       STIMULI PRESENTED ___; STABILIZED IN ___
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 0          0         2           Binocular, stabilized in both eyes (B.S.)
 0          1         7           Binocular, stabilized in right eye
 0          2         8           Binocular, stabilized in left eye
 0          3         1           Binocular, no stabilization (normal) (B.N.)

 1          0         4           Right eye, stabilized in both eyes (R.S.)
 1          1         9           Right eye, stabilized in right eye
 1          2         10          Right eye, stabilized in left eye
 1          3         3           Right eye, no stabilization (normal) (R.N.)

 2          0         6           Left eye, stabilized in both eyes (L.S.)
 2          1         11          Left eye, stabilized in right eye
 2          2         12          Left eye, stabilized in left eye
 2          3         5           Left eye, no stabilization (normal) (L.N.)


EYEC and eye_d contain the same labeling convention:
0 --> stimuli was presented binocularly
1 --> stimuli was presented to the right eye only
2 --> stimuli was presented to the left eye only


STABC and stab_d contain the same labeling convention:
0 --> stimuli was stabilized binocularly
1 --> stimuli was stabilized in the right eye only
2 --> stimuli was stabilized in the left eye only
3 --> stimulu was no stabilized, AKA 'normal'

ALL_COND is the variable that stores all of this information, specifically
created and used for the box plot

%}
%% Preprocess data

clear all
close all

% Set the path to data
subject = 'MAC';

pathtodata = ('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat');

load('Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Data/temp/results.mat')

% Set figure path

fig_path = '/Users/EminAlicic/Box Sync/EA & MAC/StereoFading/Figures/MAC';
sub_fig_path = fullfile(fig_path);
if (~isfolder(sub_fig_path))
    mkdir(sub_fig_path)
end

% Creates an empty matrix - 'slec' - and fills it with formatted
% information using EISToolbox
slec = eis_readData_binocular([], 'x1');
slec = eis_readData_binocular(slec, 'y1');
slec = eis_readData_binocular(slec, 'x2');
slec = eis_readData_binocular(slec, 'y2');

% Defines L/R triggers
slec = eis_readData_binocular(slec, 'stream', 0, 'int'); % right trigger
slec = eis_readData_binocular(slec, 'stream', 1, 'int'); % left trigger

% Stabilized eye data
slec = eis_readData_binocular(slec, 'stream', 2, 'double'); % stabilized eye 1 data x1.y1
slec = eis_readData_binocular(slec, 'stream', 3, 'double');  % stabilized eye 2 data x1.y1

% Defines blinks and no tracks
slec = eis_readData_binocular(slec, 'trigger', 'blink1');
slec = eis_readData_binocular(slec, 'trigger', 'notrack1');
slec = eis_readData_binocular(slec, 'trigger', 'blink2');
slec = eis_readData_binocular(slec, 'trigger', 'notrack2');

slec = eis_readData_binocular(slec,'photocell');
slec = eis_readData_binocular(slec,'spf');

slec = eis_readData_binocular(slec, 'uservar','SubjectName');
slec = eis_readData_binocular(slec, 'uservar','DEBUG');
slec = eis_readData_binocular(slec, 'uservar','Trial');

slec = eis_readData_binocular(slec, 'uservar','StabCond');
slec = eis_readData_binocular(slec, 'uservar','EyeCond');

slec = eis_readData_binocular(slec, 'uservar','RecalFrequency');
slec = eis_readData_binocular(slec, 'uservar','RecalIncrement');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetX1');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetY1');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetX2');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetY2');

slec = eis_readData_binocular(slec, 'uservar','PixelAngle');

%% Create vectors where information about vars is stored

info.StabCond = {'Stabilized','Right Eye Stabilized','Left Eye Stabilized','Normal'};
info.EyeCond   = {'Binocular','RightEye','LeftEye'};
info.RightStabTrace = 'stream02'; % x.y
info.LeftStabTrace  = 'stream03'; % x.y
info.StabTraceType  = 'float';
info.RightEyeIdx    = 1;
info.LeftEyeIdx     = 2;
info.codes          = getEventCodes();

em  = preprocessBinocularEM(data);

%% Create matrices for data based on conditions


EYEC = nan(1,size(data.user,1));
STABC = nan(1,size(data.user,1));
ALL_COND = nan(1,size(data.user,1));
NSWCH = nan(1,size(data.user,1));
allem = nan(1,size(data.user,1));

for tr = 1:size(data.user,1)
    
    %retrieve my data
    eye_d = double(data.user{tr}.EyeCond);
    stab_d = double(data.user{tr}.StabCond);
    ms_d = em{tr,1}.microsaccades.start;
    
    %store my data
    EYEC(tr) = eye_d;
    STABC(tr) = stab_d;
    COND(tr,:) = [data.user{tr}.EyeCond data.user{tr}.StabCond];
    
    %separate data for the box plot
    if eye_d == 0 && stab_d == 0
        ALL_COND(tr) = 2;
    elseif eye_d == 0 && stab_d == 3
        ALL_COND(tr) = 1;
    elseif eye_d == 1 && stab_d == 0
        ALL_COND(tr) = 4;
    elseif eye_d == 1 && stab_d == 3
        ALL_COND(tr) = 3;
    elseif eye_d == 2 && stab_d == 0
        ALL_COND(tr) = 6;
    elseif eye_d == 2 && stab_d == 3
        ALL_COND(tr) = 5;
    else
        ALL_COND(tr) = nan;
    end
    
    
    RT(tr,:) = any(data.stream00{tr}.data == 7);  %right trigger
    %RT.ts = data.stream01{1,1}.ts;
    
    
    LT(tr,:) = any(data.stream01{tr}.data == 7);  %left trigger
    
    tp_r = find(data.stream00{tr}.data == 7,1);
   
    % 'find' finds the first index where there is a 7 in data.stream00{tr}.data
    if isempty(tp_r)
        FadeTM_total(tr,:) = nan;
    else
        FadeTM_total(tr,:) = double(data.stream00{tr}.ts(tp_r));
        % 'double' converts to double precision array
        
        % Finds when the most recent microsaccade occurred after pressing
        % the right trigger in both eyes
        RTmsidx1(tr,:) = double(em{tr,1}.microsaccades.start(tp_r));
        RTmsidx2(tr,:) = double(em{tr,2}.microsaccades.start(tp_r));
    end
    
    tp_l = find(data.stream01{tr}.data == 7,1);
    if isempty(tp_l)
        FadeTM_partial(tr,:) = nan;
    else
        FadeTM_partial(tr,:) = double(data.stream01{tr}.ts(tp_l));
        
        % Finds when the most recent microsaccade occurred after pressing
        % the left trigger in both eyes
        LTmsidx1(tr,:) = double(em{tr,1}.microsaccades.start(tp_l));
        LTmsidx2(tr,:) = double(em{tr,2}.microsaccades.start(tp_l));
    end
    
end

%% Initialize plots, clear figures
figure('units','in','position',[0 0 1 3/4 ] *7)
clf

%% Set title strings, allocate data based on condition
for fading_cond = 1:2 % changed 'p' to 'fading_cond' to make it clear that it's referring to the fading condition (partial or total)
    
    if fading_cond == 1
        tstr =  'Total Fading';
        ystr = 'Time from Stimulus Onset to First Report of Total Fading (ms)';
        DATA = FadeTM_total;
        
    elseif fading_cond == 2
        tstr = 'Partial Fading';
        ystr =  'Time from Stimulus Onset to First Report of Partial Fading (ms)';
        DATA = FadeTM_partial;
    end
    
    % Specify the two eyes and stabilization conditions
    % need to go back in, replicate this and create a separate variable as
    % I did with STABC for example up above to fix the boxplot
    
    eyes = [1 2]; clear u
    for eye = 0:2
        
        if eye ~= 0 % Left and right eyes
            
            % Defining normal conditions in L and R eyes
            normal =  COND(:,1) == eye & ...
                (COND(:,2) == 3 | COND(:,2) == eyes(eyes~=eye));
            
            % Defining stabilized conditions in L and R eyes
            stab = COND(:,1) == eye & ...
                (COND(:,2) == eye | COND(:,2) == 0);
            
            % 'u' takes the mean across both normal and stab data
            u(eye+1,1) =  nanmean(DATA(normal));
            u(eye+1,2) =  nanmean(DATA(stab));
            
            
            % 'n' takes the number of array elements for normal and stab
            n(eye+1,1) =  numel(DATA(normal));
            n(eye+1,2) =  numel(DATA(stab));
            
        else % Binocular
            normal =  COND(:,1) == eye & ...
                (COND(:,2) == 3) ;
            
            stab = COND(:,1) == eye & ...
                ( COND(:,2) == 0);
            
            u(eye+1,1) =  nanmean(DATA(normal));
            u(eye+1,2) =  nanmean(DATA(stab));
            
            n(eye+1,1) =  numel(DATA(normal));
            n(eye+1,2) =  numel(DATA(stab));
        end
    end
    
    
    %% Bar plot
    
    figure(1)
    [p,TB] = anovan(DATA,{EYEC, STABC}, 'display', 'off'); %anova window off
    subplot(1,2,fading_cond);
    errY = 0.05*u; % 5% error
    barwitherr(errY,u); hold on
    set(gca,'box','off','tickdir','out');
    %set(gca,'XtickLabel',{'Binocular','Right Eye','Left Eye'})
    legend('Normal','Stabilized')
    ylabel(ystr);
    xlabel('Conditions');
    for eye = 1:3
        for stab = 1:2
            text(eye + -0.2 + (stab-1)/4,0, sprintf('%u',n(eye,stab)),...
                'VerticalAlignment','bottom')
        end
    end
    text(min(xlim),5,'Trial N =','VerticalAlignment','bottom','HorizontalAlignment','Left')
    str1 = sprintf('Eye Condition: F(%d, %d) = %.2f, p = %u\nStabilization: F(%d, %d) = %.2f, p = %u',...
        TB{2,3}, TB{4,3}, TB{2,6}, TB{2,7},TB{3,3}, TB{4,3}, TB{3,6}, TB{3,7});
    title({'MAC Fading Report  ' tstr});
    hold off
    
    
    %% Box plot
    
    figure(3)
    subplot(1,2,fading_cond);
    boxplot(DATA(:),ALL_COND);
    hold on
    set(gca,'box','off','tickdir','out');
    set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
    ylabel(ystr);
    xlabel('Conditions');
    str = sprintf('Eye Condition: F(%d, %d) = %.2f, p = %u\nStabilization: F(%d, %d) = %.2f, p = %u',...
        TB{2,3}, TB{4,3}, TB{2,6}, TB{2,7},TB{3,3}, TB{4,3}, TB{3,6}, TB{3,7});
    title({tstr; str});
    hold off
    
    
end


%% Eye movement analysis - how do microsaccades and drifts look when the stimuli is binocularly stabilized?

Fs = 1000; %the data was recorded on the DPI which is at 1000 Hz

%% Filtering eye movements

% Remove saccade overshoots

tms = (1:em{tr}.samples) / Fs * 1000; % time in ms

xOld_1 = em{tr,1}.x.position; % original x and y traces
yOld_1 = em{tr,1}.y.position;
xOld_2 = em{tr,2}.x.position;
yOld_2 = em{tr,2}.y.position;

trial_1 = osRemoverInEM(em{tr,1}); % remove overshoots in trial for right eye
trial_2 = osRemoverInEM(em{tr,2}); % remove overshoots in trial for left eye
xNew_1 = trial_1.x.position; % overshoot-removed x and y traces
yNew_1 = trial_1.y.position;
xNew_2 = trial_2.x.position;
yNew_2 = trial_2.y.position;



%% Removing periods of no tracks and blinks
%{
trialIdx = 97;


% Create a logical vector in which we assume that no blinks are occuring
% (false).
isblink = false(em{trialIdx}.samples);
isnotrack = false(em{trialIdx}.samples);

% loop through each blink and no track, mark its times as true
for di = 1:length(em{trialIdx}.blinks.start)
    startIdx = em{trialIdx}.blinks.start(di);
    stopIdx = startIdx + em{trialIdx}.blinks.duration(di) - 1;
    isblink(startIdx:stopIdx) = true;
end

% loop through each no track, mark its times as true
for nti = 1:length(em{trialIdx}.notracks.start)
    nt_startIdx = em{trialIdx}.notracks.start(nti);
    nt_stopIdx = em{trialIdx}.notracks.duration(nti) - 1;
    
    isnotrack(nt_startIdx:nt_stopIdx) = true;
end

xNew_1(isblink, isnotrack) = nan; % remove the values when there is a blink
yNew_1(isblink, isnotrack) = nan;
xNew_2(isblink, isnotrack) = nan;
yNew_2(isblink, isnotrack) = nan;




%% Testing if drifts are in the trial
for dri = 1:length(em{tr}.drifts.start)
    isdrift = ~isempty(em{tr}.drifts.start);
    if isdrift
        drift_start = em{tr}.drifts.start(dri);
        drift_stop = em{tr}.drifts.duration(dri) + drift_start - 1;
    end
end
%}
%%
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Follow up on the Rucci and Poletti 2010 paper
%
% In their paper, they noted that under retinal stabilization, microsaccade
% rates decreased. The purpose of the following code is to analyze what the
% rates of microsaccades looked like under binocular, left eye, and right
% stabilization compared to normal conditions.


% Plotting x and y DPI traces (binocular)

figure(); clf;
subplot(4,1,1)
plot(xNew_1, 'k');
hold on
plot(yNew_1, 'r');
plot(xNew_2, 'g');
plot(yNew_2, 'b');


xlabel('time (ms)');
ylabel('position (arcmin)');

[~,h_legend] = legend('E1 X', 'E1 Y', 'E2 X', 'E2 Y', 'Location', 'Best');
title('X and Y Binocular Traces')

xlim([0 12050])

yl = ylim;

% Testing if microsaccades are in the trial
%{
for msi = 1:length(em{tr}.microsaccades.start)
    ismsaccade = ~isempty(em{tr}.microsaccades.start);
    if ismsaccade
        msaccade_start = em{tr}.microsaccades.start(msi);
        msaccade_stop = em{tr}.microsaccades.duration(msi) + msaccade_start - 1;
        
        patch([msaccade_start, msaccade_stop, msaccade_stop, msaccade_start],...
            [yl(1), yl(1), yl(2), yl(2)],...
            [.5, .5 .5],...
            'EdgeColor', 'y',...
            'FaceColor', 'y',...
            'Facealpha', .7);
        
    end
end
%}
% Showing when triggers were pressed, when msaccades occurred, and what condition was used in paradigm

subplot(4,1,2)
stairs(FadeTM_partial);
hold on
title('Right trigger presses');
xlabel('trials');
ylabel('time (ms)');
xlim([ 0 tr]);

subplot(4,1,3)
stairs(FadeTM_total);
title('Left trigger presses');
xlabel('trials');
ylabel('time (ms)');
xlim([ 0 tr]);
%{
subplot(4,1,4)
plot(ALL_COND == 2);
hold on
plot(ALL_COND == 1);
xlabel('trials');
ylabel('condition on or off');
title('Stabilization/eye conditions');
legend('Binoc pres., binoc stab.', 'Binoc pres., no stab.', 'Location', 'Best');
xlim([0 tr]);
%plot(ALL_COND == 4);
%plot(ALL_COND == 3);
%plot(ALL_COND == 6);
%plot(ALL_COND == 5);
%}
%Key
%Stabilized or normal --> 0 or 3
%LSF/HSF --> 1 or 2
%Pressed trigger/released trigger --> 7 or 4

% L/R/B Stabilized Traces (with msaccades highlighted)
% Here, I want to find every instance where the eye was stabilized in
% either the left, right, or both eyes, and in that exact moment, find and
% plot their x and y traces.
td = nan(1,size(data.user,1));


for tr = 1:length(data.user)
    eye_d = double(data.user{tr}.EyeCond);
    stab_d = double(data.user{tr}.StabCond);
    
    if eye_d == 0 && stab_d == 0
        td(tr) = 2;
    elseif eye_d == 0 && stab_d == 3
        td(tr) = 1;
    elseif eye_d == 1 && stab_d == 0
        td(tr) = 4;
    elseif eye_d == 1 && stab_d == 3
        td(tr) = 3;
    elseif eye_d == 2 && stab_d == 0
        td(tr) = 6;
    elseif eye_d == 2 && stab_d == 3
        td(tr) = 5;
    elseif eye_d == 0 && stab_d == 1
        td(tr) = 7;
    elseif eye_d == 0 && stab_d == 2
        td(tr) = 8;
    elseif eye_d == 1 && stab_d == 1
        td(tr) = 9;
    elseif eye_d == 1 && stab_d == 2
        td(tr) = 10;
    elseif eye_d == 2 && stab_d == 1
        td(tr) = 11;
    elseif eye_d == 2 && stab_d == 2
        td(tr) = 12;
    else
        td(tr) = nan;
    end
end

% stimuli presented in left eye, stab in both eyes
for tr = 1:length(data.user)
    lx1pos_stabinoc = xNew_1(td == 6);
    lx2pos_stabinoc = xNew_2(td == 6);
    ly1pos_stabinoc = yNew_1(td == 6);
    ly2pos_stabinoc = yNew_2(td == 6);
end

if td ~= 6
    disp('There were no trials in this data set in which the stimuli was presented in the left eye and stabilized binocularly')
elseif length(lx1pos_stabinoc) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the left eye and stabilized binocularly')
else
    figure(); clf; hold on
    plot(lx1pos_stabinoc)
    plot(lx2pos_stabinoc)
    plot(ly1pos_stabinoc)
    plot(ly2pos_stabinoc)
    td6sum = sum(td(:)==6);
    xlim([1 td6sum]);
    title(['Stimuli presented in left eye, stabilized in both eyes. Trial Count =',num2str(td6sum)])
    xlabel('trial number');
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in left eye, stab in left eye only
for tr = 1:length(data.user)
    lx1pos_stabl = xNew_1(td == 12);
    lx2pos_stabl = xNew_2(td == 12);
    ly1pos_stabl = yNew_1(td == 12);
    ly2pos_stabl = yNew_2(td == 12);
end

if td ~= 12
    disp('There were no trials in this data set in which the stimuli was presented in the left eye and stabilized in the left eye only')
elseif length(lx1pos_stabl) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the left eye and stabilized in the left eye only')
else
    figure(); clf; hold on
    plot(lx1pos_stabl)
    plot(lx2pos_stabl)
    plot(ly1pos_stabl)
    plot(ly2pos_stabl)
    td12sum = sum(td(:) == 12);
    xlim([1 td12sum]);
    title(['Stimuli presented in left eye, stabilized in left eye only. Trial Count =',num2str(td12sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in right eye, stab in both eyes
for tr = 1:length(data.user)
    rx1pos_stabinoc = xNew_1(td == 4);
    rx2pos_stabinoc = xNew_2(td == 4);
    ry1pos_stabinoc = yNew_1(td == 4);
    ry2pos_stabinoc = yNew_2(td == 4);
end

if td ~= 4
    disp('There were no trials in this data set in which the stimuli was presented in the right eye and stabilized binocularly')
elseif length(rx1pos_stabinoc) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the right eye and stabilized binocularly')
else
    figure(); clf; hold on
    plot(rx1pos_stabinoc)
    plot(rx2pos_stabinoc)
    plot(ry1pos_stabinoc)
    plot(ry2pos_stabinoc)
    td4sum = sum(td(:)==4);
    xlim([1 td4sum]);
    title(['Stimuli presented in right eye, stabilized in both eyes. Trial Count =',num2str(td4sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in right eye, stab in right eye only
for tr = 1:length(data.user)
    rx1pos_stabr = xNew_1(td == 9);
    rx2pos_stabr = xNew_2(td == 9);
    ry1pos_stabr = yNew_1(td == 9);
    ry2pos_stabr = yNew_2(td == 9);
end

if td ~= 9
    disp('There were no trials in this data set in which the stimuli was presented in the right eye and stabilized in the right eye only')
elseif length(rx1pos_stabr) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the right eye and stabilized in the right eye only')
else
    figure(); clf; hold on
    plot(rx1pos_stabr)
    plot(rx2pos_stabr)
    plot(ry1pos_stabr)
    plot(ry2pos_stabr)
    td9sum = sum(td(:) == 9);
    xlim([1 td9sum]);
    title(['Stimuli presented in right eye, stabilized in right eye only. Trial Count = ', num2str(td9sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented binocularly, stab binocularly
for tr = 1:length(data.user)
    bx1pos_stabinoc = xNew_1(td == 2);
    bx2pos_stabinoc = xNew_2(td == 2);
    by1pos_stabinoc = yNew_1(td == 2);
    by2pos_stabinoc = yNew_2(td == 2);
end

if td ~= 2
    disp('There were no trials in this data set in which the stimuli was presented binocularly and stabilized binocularly')
elseif length(bx1pos_stabinoc) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented binocularly and stabilized binocularly')
else
    figure(); clf; hold on
    plot(bx1pos_stabinoc)
    plot(bx2pos_stabinoc)
    plot(by1pos_stabinoc)
    plot(by2pos_stabinoc)
    td2sum = sum(td(:)==2);
    xlim([1 td2sum]);
    title(['Stimuli presented binocularly, stabilized binocularly. Trial Count = ',num2str(td2sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented binocularly, stab right eye
for tr = 1:length(data.user)
    bx1pos_stabr = xNew_1(td == 7);
    bx2pos_stabr = xNew_2(td == 7);
    by1pos_stabr = yNew_1(td == 7);
    by2pos_stabr = yNew_2(td == 7);
end

if td ~= 7
    disp('There were no trials in this data set in which the stimuli was presented binocularly and stabilized in the right eye only')
elseif length(bx1pos_stabr) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented binocularly and stabilized in the right eye only')
else
    figure(); clf; hold on
    plot(bx1pos_stabr)
    plot(bx2pos_stabr)
    plot(by1pos_stabr)
    plot(by2pos_stabr)
    td7sum = sum(td(:)==7);
    xlim([1 td7sum]);
    title(['Stimuli presented binocularly, stabilized in right eye only. Trial Count = ', num2str(td7sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in right eye, stab left eye only
for tr = 1:length(data.user)
    rx1pos_stabl = xNew_1(td == 10);
    rx2pos_stabl = xNew_2(td == 10);
    ry1pos_stabl = yNew_1(td == 10);
    ry2pos_stabl = yNew_2(td == 10);
end

if td ~= 10
    disp('There were no trials in this data set in which the stimuli was presented in the right eye and stabilized in the left eye only')
elseif length(rx1pos_stabl) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the right eye and stabilized in the left eye only')
else
    figure(); clf; hold on
    plot(rx1pos_stabl)
    plot(rx2pos_stabl)
    plot(ry1pos_stabl)
    plot(ry2pos_stabl)
    td10sum = sum(td(:) == 10);
    xlim([1 td10sum]);
    title(['Stimuli presented in right eye, stabilized in left eye only. Trial Count = ',num2str(td10sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in left eye, stabilized in right eye
for tr = 1:length(data.user)
    lx1pos_stabr = xNew_1(td == 11);
    lx2pos_stabr = xNew_2(td == 11);
    ly1pos_stabr = yNew_1(td == 11);
    ly2pos_stabr = yNew_2(td == 11);
end

if td ~= 11
    disp('There were no trials in this data set in which the stimuli was presented in the left eye and stabilized in the right eye only')
elseif length(lx1pos_stabr) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the left eye and stabilized in the right eye only')
else
    figure(); clf; hold on
    plot(lx1pos_stabr)
    plot(lx2pos_stabr)
    plot(ly1pos_stabr)
    plot(ly2pos_stabr)
    td11sum = sum(td(:) == 11);
    xlim([1 td11sum]);
    title(['Stimuli presented in left eye, stabilized in right eye only. Trial Count = ',num2str(td11sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented binocularly, stab in left eye only
for tr = 1:length(data.user)
    bx1pos_stabl = xNew_1(td == 8);
    bx2pos_stabl = xNew_2(td == 8);
    by1pos_stabl = yNew_1(td == 8);
    by2pos_stabl = yNew_2(td == 8);
end

if td ~= 8
    disp('There were no trials in this data set in which the stimuli was presented binocularly and stabilized in the left eye only')
elseif length(bx1pos_stabl) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented binocularly and stabilized in the left eye only')
else
    figure(); clf; hold on
    plot(bx1pos_stabl)
    plot(bx2pos_stabl)
    plot(by1pos_stabl)
    plot(by2pos_stabl)
    td8sum = sum(td(:) == 8);
    xlim([1 td8sum]);
    title(['Stimuli presented binocularly, stabilized in the left eye only. Trial Count = ',num2str(ts8sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end
% L/R/B Normal Traces (with msaccades highlighted)

% stimuli presented in left eye, not stab 5
for tr = 1:length(data.user)
    lx1pos_norm = xNew_1(td == 5);
    lx2pos_norm = xNew_2(td == 5);
    ly1pos_norm = yNew_1(td == 5);
    ly2pos_norm = yNew_2(td == 5);
end

if td ~= 5
    disp('There were no trials in this data set in which the stimuli was presented in the right eye and not stabilized')
elseif length(lx1pos_norm) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the right eye and not stabilized')
else
    figure(); clf; hold on
    plot(lx1pos_norm)
    plot(lx2pos_norm)
    plot(ly1pos_norm)
    plot(ly2pos_norm)
    td5sum = sum(td(:) == 5);
    xlim([1 td5sum]);
    title(['Stimuli presented in the right eye, not stabilized. Trial Count = ',num2str(td5sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented in right eye, not stab 3
for tr = 1:length(data.user)
    rx1pos_norm = xNew_1(td == 3);
    rx2pos_norm = xNew_2(td == 3);
    ry1pos_norm = yNew_1(td == 3);
    ry2pos_norm = yNew_2(td == 3);
end

if td ~= 3
    disp('There were no trials in this data set in which the stimuli was presented in the left eye and not stabilized')
elseif length(rx1pos_norm) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented in the left eye and not stabilized')
else
    figure(); clf; hold on
    plot(rx1pos_norm)
    plot(rx2pos_norm)
    plot(ry1pos_norm)
    plot(ry2pos_norm)
    td3sum = sum(td(:) == 3);
    xlim([1 td3sum]);
    title(['Stimuli presented in the left eye, not stabilized. Trial Count = ',num2str(td3sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end

% stimuli presented binocularly, not stab 1
for tr = 1:length(data.user)
    bx1pos_norm = xNew_1(td == 1);
    bx2pos_norm = xNew_2(td == 1);
    by1pos_norm = yNew_1(td == 1);
    by2pos_norm = yNew_2(td == 1);
end


if td ~= 1
    disp('There were no trials in this data set in which the stimuli was presented binocularly and not stabilized')
elseif length(bx1pos_norm) < 2
    disp('There weren''t sufficient trials in this data set in which the stimuli was presented binocularly and not stabilized')
else
    figure(); clf; hold on
    plot(bx1pos_norm)
    plot(bx2pos_norm)
    plot(by1pos_norm)
    plot(by2pos_norm)
    td1sum = sum(td(:) == 1);
    xlim([1 td1sum]);
    title(['Stimuli presented binocularly, not stabilized. Trial Count = ', num2str(td1sum)])
    xlabel('trial number')
    ylabel('position (arcmin)');
    legend('X1', 'X2', 'Y1', 'Y2', 'Location', 'Best');
end



% Plots showing rates of microsaccades depending on stabilization,
% velocity, amplitude, angles, in which eye,
% ,etc., along with ANOVA statistics
% Microsaccade amplitude and angles

msamps_1 = em{tr,1}.microsaccades.amplitude;
msangs_1 = em{tr,1}.microsaccades.angle;
msamps_2 = em{tr,2}.microsaccades.amplitude;
msangs_2 = em{tr,2}.microsaccades.angle;
figure(); clf;
subplot(1,5,1);
histogram(msamps_1, 30);
xlabel('Right eye microsaccade amplitude (arcmin)');
ylabel('Count');

subplot(1,5,2);
histogram(msamps_2, 30);
xlabel('Left eye microsaccade amplitude (arcmin)');
ylabel('Count');

subplot(1,5,3);
polarhistogram(msangs_1,30);
title('Microsaccade directions, right eye');

subplot(1,5,4);
polarhistogram(msangs_2,30);
title('Microsaccade directions, left eye');

%%
clear  nMsaccade* nSaccade* nDrift* trialDur* rate_ms* NEWrate_ms* 
for e = 1:length(data.user)
   
    nMsaccade1(e) = length(em{e,1}.microsaccades.start);
    nSaccade1(e) = length(em{e,1}.saccades.start);
    nDrift1(e) = length(em{e,1}.drifts.start);
    trialDur1(e) = em{e,1}.samples/Fs;
    rate_ms1(e) = nMsaccade1(e) / (trialDur1(e));
    NEWrate_ms1(e) = (nSaccade1(e) + nMsaccade1(e)) / (trialDur1(e));
    
 
    nMsaccade2(e) = length(em{e,2}.microsaccades.start);
    nSaccade2(e) = length(em{e,2}.saccades.start);
    nDrift2(e) = length(em{e,2}.drifts.start);
    trialDur2(e) = em{e,2}.samples/Fs;
    rate_ms2(e) = nMsaccade2(e) / (trialDur2(e));
    NEWrate_ms2(e) = (nSaccade2(e) + nMsaccade2(e)) / (trialDur2(e));

end

clear em1 em2
em1 = [nMsaccade1(:) nSaccade1(:) trialDur1(:) rate_ms1(:)  nDrift1(:) NEWrate_ms1(:)];
em2 = [nMsaccade2(:) nSaccade2(:) trialDur2(:) rate_ms2(:)  nDrift2(:) NEWrate_ms2(:)];

clear dat
dat = [em1(:,4), em2(:,4)];

clear TBEM*
[~,TBEM1] = anovan(dat(~isnan(ALL_COND),1),{EYEC(~isnan(ALL_COND)), STABC(~isnan(ALL_COND))}, 'display', 'off','model','interaction'); %anova window off
[~,TBEM2] = anovan(dat(~isnan(ALL_COND),2),{EYEC(~isnan(ALL_COND)), STABC(~isnan(ALL_COND))}, 'display', 'off','model','interaction'); %anova window off


%rate_ms1(:)
%rate_ms2(:)
figure(); cla; hold on
subplot(1,2,1)
boxplot(dat(:,1),ALL_COND);
set(gca,'box','off','tickdir','out');
set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
ylabel('Rate of Microsaccades (Microsaccades/se Across Trials)');
xlabel('Conditions');
str = sprintf('Eye Condition: F(%d, %d) = %.2f, p = %.3f\nStabilization: F(%d, %d) = %.2f, p = %.3f\nInteraction:  F(%d, %d) = %.2f, p = %.3f',...
    TBEM1{2,3}, TBEM1{5,3}, TBEM1{2,6}, TBEM1{2,7},TBEM1{3,3}, TBEM1{5,3}, TBEM1{3,6}, TBEM1{3,7}, TBEM1{4,3}, TBEM1{5,3}, TBEM1{4,6}, TBEM1{4,7});
tstr1 = 'Right Eye';
title({tstr1; str});

subplot(1,2,2)
boxplot(dat(:,2),ALL_COND);
set(gca,'box','off','tickdir','out');
set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
ylabel('Rate of Microsaccades (Microsaccades/s Across Trials');
xlabel('Conditions');
str = sprintf('Eye Condition: F(%d, %d) = %.2f, p = %.3f\nStabilization: F(%d, %d) = %.2f, p = %.3f\nInteraction:  F(%d, %d) = %.2f, p = %.3f',...
    TBEM2{2,3}, TBEM2{5,3}, TBEM2{2,6}, TBEM2{2,7},TBEM2{3,3}, TBEM2{5,3}, TBEM2{3,6}, TBEM2{3,7}, TBEM2{4,3}, TBEM2{5,3}, TBEM2{4,6}, TBEM2{4,7});
tstr2 = 'Left Eye';
title({tstr2; str});

%% Create an table that shows when the most recent microsaccade occurred when a subject reported fading

trialNum = (linspace(1,tr,tr))';

conditions = struct([]);
count = 1;
for i = 1:length(data.user)
    
    trialCount = num2cell(trialNum);
    [conditions.trialCount] = trialCount{:};
    
    conditions(i).total_msacc1 = length(em{i,1}.microsaccades.start); %total msacc count in right eye
    %conditions(i).msacc1_ts = em{i,1}.microsaccades.start; %msacc time stamps, right eye
    conditions(i).total_msacc2 = length(em{i,2}.microsaccades.start); %total msacc count in left eye
    %conditions(i).msacc2_ts = em{i,2}.microsaccades.start; %msacc time stamps, left eye
    conditions(i).total_msaccall = length(em{i,1}.microsaccades.start) + length(em{i,2}.microsaccades.start); %total msacc count in both eyes
    
    Rms1 = num2cell(RTmsidx1);
    Rms2 = num2cell(RTmsidx2);
    Lms1 = num2cell(LTmsidx1);
    Lms2 = num2cell(LTmsidx2);
    [conditions.RTmsidx1] = Rms1{:}; %the ms start ts at the first report of fading, RT, eye1
    [conditions.RTmsidx2] = Rms2{:}; %RT, eye2
    [conditions.LTmsidx1] = Lms1{:}; %LT, eye1
    [conditions.LTmsidx2] = Lms2{:}; %LT, eye2
    
end

%struct2csv(conditions,'fading_msanalysis.csv');

sac2_1 = sum(nSaccade1(ALL_COND == 2));
sac1_1 = sum(nSaccade1(ALL_COND == 1));
sac4_1 = sum(nSaccade1(ALL_COND == 4));
sac3_1 = sum(nSaccade1(ALL_COND == 3));
sac6_1 = sum(nSaccade1(ALL_COND == 6));
sac5_1 = sum(nSaccade1(ALL_COND == 5));
%{
[a1,~,c1] = unique(ALL_COND);
A1 = accumarray(c1(:),nDrift2(:));
bar(a1(:),A1(:))
title('Total Drifts Per Condition'); hold on
ylabel('Drift Count');
xlabel('Conditions');
set(gca,'box','off','tickdir','out');
set(gca,'XtickLabel',{'Binoc Norm','Binoc Stab.','R Norm','R Stab','L Norm','L Stab'})
xlim([0.5 6.5]);
%}


%% Save figures
%{
saveas(figure(1),[sub_fig_path 'fade_barplot.jpg']);
saveas(figure(2), [sub_fig_path 'fade_boxplot.jpg']);
saveas(figure(3), [sub_fig_path 'xytrace_mshighlight.jpg']);
saveas(figure(4), [sub_fig_path 'trace_lbstab.jpg']);
saveas(figure(5), [sub_fig_path 'trace_rbstab.jpg']);
saveas(figure(6), [sub_fig_path 'trace_bbstab.jpg']);
saveas(figure(7), [sub_fig_path 'trace_brstab.jpg']);
saveas(figure(8), [sub_fig_path 'trace_rn.jpg']);
saveas(figure(9), [sub_fig_path 'trace_ln.jpg']);
saveas(figure(10), [sub_fig_path 'trace_bn.jpg']);
saveas(figure(11), [sub_fig_path 'ms_amps_angs.jpg']);
saveas(figure(12), [sub_fig_path 'msrates_boxplot.jpg']);
%}


















